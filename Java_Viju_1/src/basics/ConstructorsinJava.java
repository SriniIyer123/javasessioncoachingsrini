package basics;
//super, static, this

public class ConstructorsinJava {

	//Instance variables
	String name;
	int a;
	static char c;//shared by class for all objects
//1. to initialize instance variable
//2. gets called when we create an object
//3. its a method whose name is same as classname	
//4. It doesn't have a return type
	
	void displayVal()
	{
		System.out.println("Name: "+name+" a: "+a+" c: "+c);
	}
	//overrides the default implementation of NonParameterized Constructor
	ConstructorsinJava()
	{
		//fixed values
		a=0;
		name="";
		c='c';
	}
	
	ConstructorsinJava(String name,int a,char c)
	{
		//fixed values
		this.a=a;
		this.name=name;
		this.c=c;
	}
//Method Overloading
	ConstructorsinJava(int a1,char c1,String name1)
	{
		//fixed values
		a=a1;
		name=name1;
		c=c1;
	}
	public static void main(String[] args) {
		ConstructorsinJava Obj=new ConstructorsinJava();
		ConstructorsinJava Obj1=new ConstructorsinJava("Sri",20,'a');
		ConstructorsinJava Obj2=new ConstructorsinJava(30,'c',"Srinii");
		Obj.displayVal();
		Obj1.displayVal();
		Obj2.displayVal();
	}

}
