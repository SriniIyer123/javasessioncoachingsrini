package basics;

import java.util.ArrayList;

public class ConditionalStatements {
	int[] rollnumbers= {1,2,3,4,5};//declare and instantiate at same time
	                  //0,1,2,3,4
	int[] rollnumbers2=new int[5];//allocated memory and mentioned size
	
	int[][] telnos= {{1,2},{2,3}}; //2*2 Array
	                //0,1= rows, 0,1
	
	String name;
	final double TotalMaxmarks=300.00;//final=> value of the variable cannot be changed, constant
	double marks1,marks2,marks3,percentage;
	String grade;//output to be computed
	static int totalval=0;
	
	void print1dArr()
	{
		System.out.println("This is 1dArray");
		for(int i=0;i<rollnumbers.length;i++)
		{
			System.out.println("Value:"+i+" is: "+rollnumbers[i]);
		}
	}
	
	void print2dArr()
	{
		System.out.println("This is 2dArray");
		for(int i=0;i<telnos.length;i++)//rows=>2 i=0
		 for(int j=0;j<telnos[0].length;j++)//cols j=0,1
			System.out.println("Value of telnos:"+"i:"+i+"j:"+j+" is: "+telnos[i][j]);		
	}

	double findTotal(double marks1, double marks2, double marks3)
	{
		return marks1+marks2+marks3;		
	}

	public String getGrade(double totalObtmarks)
	{
		percentage=(totalObtmarks/TotalMaxmarks)*100;
		if(percentage >= 80.00)//condition check
		{
	      grade="Distinction";//action flow
		}
		else if(percentage >=65.00 && percentage <80.00)
		{
			grade="1st Class";
		}
		else if(percentage >=50.00 && percentage <65.00)
		{
			grade="2nd Class";
		}
		else if(percentage < 50.00)
		{
			grade="Fail";
		}
		else
		{
			System.out.println("Percentage can't be computed");
		}
		return grade;
	}
	
	public void demoforLoop()
	{
		for(int i=0;i>10;i++)//initialization-block;condition block;increment/decrement block
		{
			System.out.println("i:"+i);
		}
	}
	
	public void demowhileLoop()
	{
		    int i=5;
		    while(i>0)
			{
			   System.out.println("i in while:"+i);
			   i--;
			}	
		    
		    i=-1;
		    do{
		    	System.out.println("i in do-while:"+i);
				i--;
		    }while(i>0);
	}
	
	public void foreachDemo()
	{
		int counter=1;
		int[] arr= {1,2,3,4,5};
		for(int i:arr)//DataType <variable_name>:<Container>{...}
		{
			System.out.println("i:"+i);
		}
		ArrayList<String> arrlist=new ArrayList<String>();
		arrlist.add("Srini");
		arrlist.add("Deepak");
		for(String str:arrlist)
		{
			System.out.println("Element:"+counter+str);
			counter++;
		}		
		
		arrlist.forEach((element)->System.out.println(element));//lambda function (d)->System.out.println(d)
	}
	
	
	
	public void getComments(String grade)
	{
		switch(grade)//grade=> can be String or char
		{
		case "Distinction":
			System.out.println("Thts an excellent grade, Congratulations");			
	        break;
		case "1st Class":	
             System.out.println("Thts a a good grade, Congratulations");			
			break;
		case "2nd Class":
			 System.out.println("Thts an ok grade, Congratulations");
			 break;
		case "Fail":
			System.out.println("Thts fail grade, Need to improve");
			break;
		}
		
	}
	/* @Author:
	 * Purpose: check usage of break and continue
	 * Date:
	 * Static methods can only access static instance variables or call static methods
	 */
	public static void usageofBreakContinue(int a)//a=2
	{
		System.out.println("Totalval:"+totalval);
		//System.out.println("Name is:"+name);
		if(a==1) {
			System.out.println("a==1");
		}
		else if(a>1)
		{
			for(int j=0;j<=a;j++) //0 to 2
			{
				if(j==1) {
					continue;//label: continue and it helps to skip remaining statements and continue next iteration if a condition is met
				/* continue should be used when we want to skip remaining statements until our condition is met  
				 * 
				 */
				}
				else if(j==2)
					break;//label: break, break a loop if our reqt is met so that we dont waste time in further iterations
				System.out.println("In iteration:"+j+"Value of a: "+a);	
				a++;//2++(1)
			}
			System.out.println("Final value of a:"+a);
		}		
	}
	
	public static void main(String[] args) {
		double total=0.00;//declaration and instantiation
		ConditionalStatements Students=new ConditionalStatements();
		Students.name="Viju";
		total=Students.findTotal(80.00, 90.00, 90.00);
		System.out.println("Grade of student: "+Students.name+" is: "+Students.getGrade(total));
		//Students.print1dArr();
		//Students.print2dArr();
		Students.foreachDemo();
		//Students.usageofBreakContinue(3);
		/*Students.getComments(Students.grade);
		Students.demoforLoop();
        Students.demowhileLoop();*/
	}

}
