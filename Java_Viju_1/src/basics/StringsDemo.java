package basics;

import java.util.stream.IntStream;

//Data types: int,long, double,short,float,char, boolean(Primitive data types)
//Non-Primitive data types: Array, Class, Obj, Strings(Immutable in nature)




public class StringsDemo {

	public void returnVal(String str1, String str2)
	{
		
		String str="Deepak";//=> stored in another area not i
		String str9=new String("Deepak");//create in heap memory and in constant pool(if not present already)
        String str10="Deepak";//this wont again in string constant pool
        String str11=new String("Deepak");
        str10=str10+"is good in Java";
        str=str10;//reference  =
        
		if(str1==str2)//comparing references or memory index and not values
			System.out.println("str1 and str2 are equal in references");
		else
			System.out.println("str1 and str2 are not equal in references");
		
		if(str1.equals(str2))//checking values
			System.out.println("str1 and str2 are equal");
		else
			System.out.println("str1 and str2 are not equal");
	}
	
	public void Stringmethods(String str,String str2)
	{
		System.out.println("Char at index:0 is:"+str.charAt(11));//StringIndexOutofBoundsException		
	    IntStream obj=str.chars();
	    obj.forEach((d)-> System.out.println(d));	
	    System.out.println("str compared to str2?:"+str.compareTo(str2));
	    str=str.concat(str2);
	    System.out.println("str now:"+str);
	    System.out.println("Does str contains Vijay string?:"+str.contains("Vijay"));
	    System.out.println("Index of l:"+str.indexOf('l'));
	    System.out.println(str.indexOf("iVijay"));
	    System.out.println(str.indexOf('l', 6));
	    System.out.println(str.substring(12,14));//14 is always excluded
	    
	    String str3="Vijaylakshmi learns Java well";
	    str3=str3.trim();//trailing spaces, leading spaces	    
	    char[] ch=str3.toCharArray();
	    for(int i=0;i<ch.length;i++)
	    {
	    	if(ch[i]==' ')
	    		ch[i]='\0';	  
	    	
	    }

	   String str4= ch.toString();//converts char[] to String
	   System.out.println("str4 after converting char array to String: "+str4);
	    String[] strarr=str3.split(" ");
	    /*StringBuffer strbuf=new StringBuffer(str3);
	    strbuf.reverse();*/
	    
	    StringBuffer totalval=new StringBuffer("");//mutable- synchronized(wait for lock to be released on a thread)
	    StringBuilder totalval2=new StringBuilder("");//mutable-non-synchronized(wont wait)-> in parallel- more faster
	    for(int i=0;i<strarr.length;i++)
	    {
	    	System.out.println(strarr[i]);
	        totalval.append(strarr[i]); //+=	        
	    }
	    System.out.println("Final value without spaces is:"+totalval);
	}
	
	public static void main(String[] args) 
	{
		StringsDemo obj=new StringsDemo();
		String str="Vijaylakshmi";//0 to length-1 0 to 11
		String str2="Vijaylakshmi";
		String str1=new String("Vijaylakshmi");
		str2=new String("Vijaylakshmi");
		//obj.returnVal(str,str2);   
		//str="Vijaylakshmi1";
		obj.returnVal(str1,str2);
		//str2="Vijaylakshmi1";
		obj.Stringmethods("Vijaylakshmi","Vijay");//0 to 11
	}

}
