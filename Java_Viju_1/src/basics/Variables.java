package basics;

//Student() => Constructor -default constructor

public class Variables {

	String students;//instance variable
	int obj=10;//instance of class variable, obj is called as reference variable and 10 is the value
	static String schoolname="St Marys High School";//static variable=> belongs to class
	
	public void m2()//instance method
	{
		int ab=0;//local variable
		obj=obj+20;
		System.out.println("Object: "+obj);//+ => Concatenation for strings with other variables or objects
	}
	
	public static void m3()
	{
		System.out.println("I am static method");
	}
	public int add(int a, int b)//Receiving parameters
	{
		return a+b;
	}
	public static void main(String args[])
	{
		int a=10;//int
		char c='c';//char
		boolean condtncheck=false;//boolean
		float fl=0.9F;//float
		double d1=0.9;
		long l1=2388888888L;
		short sh=(short)2345;
		Variables stud=new Variables();//method declaration
		stud.obj=20;
		System.out.println("d1:"+(++d1));
		System.out.println("d1 later:"+d1);
		stud.m2();
		System.out.println("Addition of 2 numbers is: "+stud.add(a,sh));//int short
		System.out.println(Variables.schoolname);//static variable
		Variables.m3();
		System.out.println(stud.obj);//=20;
		System.out.println("Is stud instance of Variables class?");
		System.out.println(stud instanceof Variables);
		//Operators
		//Arithmetic Operators  + , - , /, *, %
		//Relational Operators  >,<,!=,==,>=,<=, instanceof(Inheritance)
		//Assignment Operators  =,+=,-=,*=,/=,%=,&=
		//Logical Operators && ||
		//Ternary Operators (a>b)?c:d
		//Unary Operators ++a, a++, --a,a--
		//  , Exclusive OR, Exclusive AND
		//01001 bin num1
		//01111 bin num2
		//01001
		//00110 o/p
	}
	
	//f1=10;

}
