package basics;

public class StaticDemo {

	static int a=10;//static instance variable
    int a1=20;//non-static instance variable
    
	//1st
	static {
		a=20;
		System.out.println("This is static block");
		System.out.println("hello this is updated");
	}
	
	//2nd
	StaticDemo()
	{
		System.out.println("Non parameterized Const");
	}
	
	//non-static method can access both static and non-static
	void m2()
	{
		a1=30;
		a=20;	
	}
	//3rd //static can only access static variable they cant access non-static
	static void printa()
	{
		int b=20;
		System.out.println("This is a static method and a is:"+a);
		//a1=30; Can't access non-static variable.
	}
	
	public static void main(String[] args) {
		StaticDemo obj=new StaticDemo();
		StaticDemo obj1=new StaticDemo();
		StaticDemo obj2=new StaticDemo();
		//System.out.println("Obj1a:"+obj1.a+" Obj2.a: "+obj2.a+"obj.a: "+obj.a);
		//obj.a=20;
		//System.out.println("Obj1a:"+obj1.a+" Obj2.a: "+obj2.a+"obj.a: "+obj.a);
		printa();
		//a1=40;//calling/accessing a non static variable from main method will throw an error
	    //Non-static variables can be accessed via non-static methods, objects of class but cant acces via static method directly
		int a=15;
		a=40;
		a=25;
	}

}
