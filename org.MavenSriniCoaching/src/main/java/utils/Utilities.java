package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import config.ConstantsFile;
import pages.BasePage;

//child extending parent
public class Utilities extends BasePage{

	public Utilities() {
		// TODO Auto-generated constructor stub
	}
	
	
	public WebElement formLocator(By byValue)
	{
		return driver.findElement(byValue);
	}
	
	public String getPageTitle()
	{
		return driver.getTitle();
	}
	
	public void clickElement(WebElement element)
	{
		element.click();
	}
	
	public void sendKeys(WebElement element, String sendkeys)
	{
		element.sendKeys(sendkeys);
	}

	public boolean isElementDisplayed(WebElement element)
	{
		return element.isDisplayed();
	}
}
