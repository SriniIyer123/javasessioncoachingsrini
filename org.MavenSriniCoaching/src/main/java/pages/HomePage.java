package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

//Page Object Model- locators for every page, and methods to interact with those locators
//Page Factory=>Rather than using By, we use WebElement using how , constructor=>loading all the WebElements of the page

public class HomePage extends BasePage {

	//locators	-> Object Repository=> approach for object repository
	By menu_SignUp=By.xpath("//a[text()='Sign Up']/parent::li");
	By menu_About=By.linkText("About");
	By menu_CRM=By.linkText("CRM");
	By menu_APPS=By.linkText("Apps");
	By menu_Compare=By.linkText("Compare");
	By menu_Pricing=By.linkText("Pricing");
	By menu_LogIn=By.xpath("//span[text()='Log In']/parent::a");

	By SignUp=By.xpath("//span[text()=' sign up']/parent::a");
	
		
	public HomePage() {
		super();//this will call Superclass Constructor
	}	
	
	public RegisterPage clickSignUp()
	{
		WebElement SignUp=driver.findElement(menu_SignUp);
		SignUp.click();
		return new RegisterPage();
	}

	public void clickAbout()
	{
		WebElement About=driver.findElement(menu_About);
		About.click();
	}
	
	public void clickCRM()
	{
		WebElement CRM=driver.findElement(menu_CRM);
		CRM.click();
	}
	
	public void clickAPPS()
	{
		WebElement APPS=driver.findElement(menu_APPS);
		APPS.click();
	}
	
	public void clickCompare()
	{
		WebElement Compare=driver.findElement(menu_Compare);
		Compare.click();
	}

	public void clickPricing()
	{
		WebElement Pricing=driver.findElement(menu_Pricing);
		Pricing.click();
	}
	
	public void clickLogin()
	{
		WebElement Login=driver.findElement(menu_LogIn);
		Login.click();
	}
	
}
