package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import utils.Utilities;

public class RegisterPage extends BasePage{
Utilities utilities=new Utilities();
//Page Factory

public RegisterPage() {
	super();
	PageFactory.initElements(driver, this);//load elements of this page
}

//1st way: using how
@FindBy(how=How.ID,using="email")
WebElement EmailAddress;

//2nd way:using individual locator types: className, xpath, name, linkText, partialLinkText, css, tagName
@FindBy(id="phone_number")
WebElement PhoneNumber;

@FindBy(xpath="//h2[text()='Register']")
WebElement RegisterLabel;

@FindBy(partialLinkText="terms and conditions")
WebElement TermsandConditions;

public void enterEmailAddress(String EmailAddressval)
{
  utilities.sendKeys(EmailAddress, EmailAddressval);	 
}

public void enterPhoneNumber(String PhoneNumberval)
{
	 utilities.sendKeys(PhoneNumber, PhoneNumberval);	 
}

public boolean checkifRegisterLabelDisplayed()
{
	boolean value=utilities.isElementDisplayed(RegisterLabel);//=>!(true)=> false
	return value;
}

public TermsnConditions acceptTermsandConditions()
{
	utilities.clickElement(TermsandConditions);
	return new TermsnConditions();
}


/* Page Object Model: Below approach to be used
	By EmailAddress=By.id("email");
	By PhoneNumber=By.id("phone_number");
	By RegisterLabel=By.xpath("//h2[text()='Register']");
	
	public void enterEmailAddress(String EmailAddressval)
	{
	  WebElement EmailAddressEl=utilities.formLocator(EmailAddress);
	  utilities.sendKeys(EmailAddressEl, EmailAddressval);	 
	}
	
	public void enterPhoneNumber(String PhoneNumberval)
	{
		 WebElement PhoneNumberel=utilities.formLocator(PhoneNumber);
		 utilities.sendKeys(PhoneNumberel, PhoneNumberval);	 
	}
	
	public boolean checkifRegisterLabelDisplayed()
	{
		WebElement RegisterLabelel=utilities.formLocator(RegisterLabel);
		boolean value=utilities.isElementDisplayed(RegisterLabelel);
		return value;
	}
*/

}
