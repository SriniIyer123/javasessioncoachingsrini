package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import config.ConstantsFile;
import utils.Utilities;

/* driver, methods to launch browser, methods to initialize properties file, and 
 * utilize properties file to get attribute values.
 * 
 */
//Parentclass
public class BasePage {
public static WebDriver driver;
static Properties prop;
static File file=null;
static FileInputStream fip=null;

//Path of Chrome Driver, iedriver, FirefoxDriver

public WebDriver getDriver()
{
	return driver;
}

public static void initializeProperties(String FilePath)
{
	try {
	    file=new File(FilePath);//ConstantsFile.ConfigPropertiesPath);		
		fip=new FileInputStream(file);//FileReader fileread
		prop=new Properties();
		prop.load(fip);//Loads the inputstream by properties file object			
	} catch (FileNotFoundException e) {
		System.out.println("FileNotFoundException caught in initializeProperties:"+e.getMessage());
	} 
	catch(Exception e)
	{
		System.out.println("Exception caught in initializeProperties:"+e.getMessage());
	}		
}

//above method to be called first
public static String readProperty(String propertyName)
{
	return prop.getProperty(propertyName);
}

public static void setUp()//BeforeMethod, BeforeTest
{
	String browser=readProperty(ConstantsFile.BrowserName);
	String ChromeDriverPath=System.getProperty("user.dir")+readProperty(ConstantsFile.RelativeChromeDriverPath);
	String ApplicationUrl=readProperty(ConstantsFile.ApplicationUrl);
	System.out.println("Using browser:"+browser);
	System.out.println("Made changes");
	switch(browser)
	{
	
	case "chrome":
		System.setProperty("webdriver.chrome.driver",ChromeDriverPath);
		driver=new ChromeDriver();
		
	break;
	
	case "ie":
		
	break;
	
	case "firefox":
		
	break;
	}
	
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	driver.get(ApplicationUrl);
}

public void quitApp()
{
	driver.quit();
}

	public BasePage() {
		//setUp();
	}

}
