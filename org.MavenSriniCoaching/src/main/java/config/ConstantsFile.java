package config;
//WebDriverManager(Automatically it picks the driver based on the browser).
public class ConstantsFile {

    /*final public static String BrowserName="chrome";
	final public static String ChromeDriverPath= System.getProperty("user.dir")+"//src//main//resources//drivers//chromedriver.exe";	
	final public static String ExcelTestdataPath="";
	final public static String ApplicationUrl="https://freecrm.co.in/";
	*/
	final public static String ConfigPropertiesPath=System.getProperty("user.dir")+"//src//main//java//config//config.properties";
	final public static String ORPropertiesPath=System.getProperty("user.dir")+"//src//main//java//OR.properties";
	final public static String BrowserName="BrowserName";
	final public static String RelativeChromeDriverPath="RelativeChromeDriverPath";
	final public static String ApplicationUrl="ApplicationUrl";
}
