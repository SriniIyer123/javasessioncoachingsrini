package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import config.ConstantsFile;
import pages.BasePage;
import utils.Utilities;

public class AbstractTest {
//BasePage basepg=new BasePage();
//Utilities utils=new Utilities();
	
@BeforeMethod
public void preReq()
{
	BasePage.setUp();
}

@AfterMethod
public void postTests()
{
	//basepg.quitApp();
}

@AfterTest
public void afterTest()
{
	
}

@BeforeTest
public void b4Test()
{
	
}

@BeforeSuite
public void b4Suite()
{
	BasePage.initializeProperties(ConstantsFile.ConfigPropertiesPath);
}
	
	
	public AbstractTest() {
		// TODO Auto-generated constructor stub
	}

}
