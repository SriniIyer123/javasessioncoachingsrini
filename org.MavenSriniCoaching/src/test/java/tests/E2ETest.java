package tests;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import pages.HomePage;
import pages.RegisterPage;
import pages.SignInPage;
import pages.TermsnConditions;
import utils.Utilities;

//user comes to website=>clicks signup=>login
public class E2ETest extends AbstractTest{
	HomePage homepg=new HomePage();
	RegisterPage signup;
	SignInPage signin;
	Utilities utils=new Utilities();
	TermsnConditions terms;

	//Assert(HardAssert, hardcheck on value=>if value matches then teststeps will continue else stops testcase) 
	//and SoftAssert(verify)=> it will still continue testcase execution
	
	 Logger log;
	 ExtentReports extentrept;
	 ExtentTest extenttest;
	 
		@BeforeMethod
		public void b4meth()
		{
			
			log=Logger.getLogger("E2ETest");
			//BasicConfigurator.configure();
			String filepath=System.getProperty("user.dir")+"\\src\\main\\java\\logs\\log4j.properties";
			System.out.println("Filepath:"+filepath);
			PropertyConfigurator.configure(filepath);
			//PropertyConfigurator.configure
			System.out.println("In b4method");
			
			extentrept= new ExtentReports(System.getProperty("user.dir")+"//test-output//ExtentReport.html");
			extenttest=extentrept.startTest("TestRun");
		}

		@AfterMethod
	    public void aftermeth()
	    {
			System.out.println("In aftermethod");
			extentrept.endTest(extenttest);
			extentrept.flush();		
	    }
	
	@Test
	public void performE2E()
	{
		try {
			SoftAssert softassrt=new SoftAssert();
			signup=homepg.clickSignUp();
			log.info("In performE2E Test method");
			System.out.println();
			System.out.println("Page Title"+utils.getPageTitle());	
			System.out.print("Is Register Label displayed?:");
			boolean flag=signup.checkifRegisterLabelDisplayed();
			softassrt.assertTrue(flag);			
			System.out.println("Is Register Label Displayed?:"+flag);
			signup.enterEmailAddress("srinii@gmail.com");
			signup.enterPhoneNumber("02833344555");
			terms=signup.acceptTermsandConditions();
			softassrt.assertAll();
			}
			catch(Exception e)
			{
				System.out.println("Exception caught in basicHomePageTest:"+e.getMessage());
			}
		    catch(AssertionError assertErr)
		   {
		    	System.out.println("AssertionError caught in basicHomePageTest:"+assertErr.getMessage());	
	 	   }
	}
	
	
	public E2ETest() {
		// TODO Auto-generated constructor stub
	}

}
