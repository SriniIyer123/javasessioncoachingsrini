package tests;
import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TestMaven {
 Logger log;
 ExtentReports extentrept;
 ExtentTest extenttest;
 
	@BeforeMethod
	public void b4meth()
	{
		
		log=Logger.getLogger("qalog");
		BasicConfigurator.configure();
		String filepath=System.getProperty("user.dir")+"\\src\\main\\java\\logs\\log4j.properties";
		System.out.println("Filepath:"+filepath);
		PropertyConfigurator.configure(filepath);
		//PropertyConfigurator.configure
		System.out.println("In b4method");
		
		extentrept= new ExtentReports(System.getProperty("user.dir")+"//test-output//ExtentReport.html");
		extenttest=extentrept.startTest("TestRun");
	}
	

	@Test
	public void TestRun()
	{
		System.out.println("In Test method"+Thread.currentThread().getId());
		log.info("info message");
		extenttest.log(LogStatus.INFO,"INFO");
	}
	
	@Test
	public void TestRun1()
	{
		System.out.println("In Test method"+Thread.currentThread().getId());
		log.info("info message");
		extenttest.log(LogStatus.INFO,"INFO");
	}
	
	@AfterMethod
    public void aftermeth()
    {
		System.out.println("In aftermethod");
		extentrept.endTest(extenttest);
		extentrept.flush();		
    }
	
}
