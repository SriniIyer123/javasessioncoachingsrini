$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ZohoAppltn.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: your.email@your.domain.com"
    },
    {
      "line": 2,
      "value": "#Keywords Summary :"
    },
    {
      "line": 3,
      "value": "#Feature: List of scenarios."
    },
    {
      "line": 4,
      "value": "#Scenario: Business rule through list of steps with arguments."
    },
    {
      "line": 5,
      "value": "#Given: Some precondition step"
    },
    {
      "line": 6,
      "value": "#When: Some key actions"
    },
    {
      "line": 7,
      "value": "#Then: To observe outcomes or validation"
    },
    {
      "line": 8,
      "value": "#And,But: To enumerate more Given,When,Then steps"
    },
    {
      "line": 9,
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e"
    },
    {
      "line": 10,
      "value": "#Examples: Container for s table"
    },
    {
      "line": 11,
      "value": "#Background: List of steps run before each of the scenarios"
    },
    {
      "line": 12,
      "value": "#\"\"\" (Doc Strings)"
    },
    {
      "line": 13,
      "value": "#| (Data Tables)"
    },
    {
      "line": 14,
      "value": "#@ (Tags/Labels):To group Scenarios"
    },
    {
      "line": 15,
      "value": "#\u003c\u003e (placeholder)"
    },
    {
      "line": 16,
      "value": "#\"\""
    },
    {
      "line": 17,
      "value": "## (Comments)"
    },
    {
      "line": 18,
      "value": "#Sample Feature Definition Template"
    }
  ],
  "line": 20,
  "name": "Testing Zoho Application Home Page Features",
  "description": "I want to use this template for my Zoho application",
  "id": "testing-zoho-application-home-page-features",
  "keyword": "Feature",
  "tags": [
    {
      "line": 19,
      "name": "@functional"
    }
  ]
});
formatter.scenario({
  "comments": [
    {
      "line": 23,
      "value": "#@functional"
    }
  ],
  "line": 24,
  "name": "Perform Positive scenario of Sign Up on Zoho Application",
  "description": "",
  "id": "testing-zoho-application-home-page-features;perform-positive-scenario-of-sign-up-on-zoho-application",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 25,
      "value": "#glue\u003d\u003eStepDefinition"
    },
    {
      "line": 26,
      "value": "#Given Launch the browser \"chrome\""
    },
    {
      "line": 27,
      "value": "#And  Open the Zoho application \"https://www.zoho.com\""
    },
    {
      "line": 28,
      "value": "#When User clicks on Free Sign Up button \"//a[text()\u003d\u0027Free Sign Up\u0027]\""
    },
    {
      "line": 29,
      "value": "#Then User gets redirected to SignUpPage \"Create New Account - zoho.com\""
    },
    {
      "line": 30,
      "value": "#And User enters \"//*[@id\u003d\u0027emailfield\u0027]\" and \"sriniiyer861@gmail.com\" and \"//*[@id\u003d\u0027password\u0027]\" and \"Test1234\" and \"//*[@id\u003d\u0027signup-termservice\u0027]\" in SignUp form"
    },
    {
      "line": 31,
      "value": "#And User quits the browser"
    },
    {
      "line": 33,
      "value": "#User enters \\\"(.*)\\\" and \\\"(.*)\\\" and \\\"(.*)\\\" and \\\"(.*)\\\" and \\\"(.*)\\\" in SignUp form"
    },
    {
      "line": 35,
      "value": "#Scenario:  Perform Positive scenario of Sign Up on Zoho Application with Data Table"
    },
    {
      "line": 36,
      "value": "#glue\u003d\u003eStepDefinition"
    },
    {
      "line": 37,
      "value": "#Given Launch the browser \"chrome\""
    },
    {
      "line": 38,
      "value": "#And  Open the Zoho application \"https://www.zoho.com\""
    },
    {
      "line": 39,
      "value": "#When User clicks on Free Sign Up button \"//a[text()\u003d\u0027Free Sign Up\u0027]\""
    },
    {
      "line": 40,
      "value": "#Then User gets redirected to SignUpPage \"Create New Account - zoho.com\""
    },
    {
      "line": 41,
      "value": "#And User enters details in signup form"
    },
    {
      "line": 42,
      "value": "#     | //*[@id\u003d\u0027emailfield\u0027] | sriniiyer861@gmail.com | //*[@id\u003d\u0027password\u0027] | Test1234 | //*[@id\u003d\u0027signup-termservice\u0027] |"
    },
    {
      "line": 43,
      "value": "#And User quits the browser"
    },
    {
      "line": 44,
      "value": "#"
    }
  ],
  "line": 46,
  "name": "Use multiple data sets to signup on Zoho.com",
  "description": "",
  "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 45,
      "name": "@functional"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "Launch the browser \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "Open the Zoho application \"https://www.zoho.com\"",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "User clicks on Free Sign Up button \"//a[text()\u003d\u0027Free Sign Up\u0027]\"",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "User gets redirected to SignUpPage \"Create New Account - zoho.com\"",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "User enters data sets \"//*[@id\u003d\u0027emailfield\u0027]\" and \u003cemailValue\u003e and \"//*[@id\u003d\u0027password\u0027]\" and \u003cpasswordValue\u003e and \"//*[@id\u003d\u0027signup-termservice\u0027]\" in SignUp form",
  "keyword": "And "
});
formatter.examples({
  "line": 52,
  "name": "",
  "description": "",
  "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com;",
  "rows": [
    {
      "cells": [
        "emailValue",
        "passwordValue"
      ],
      "line": 53,
      "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com;;1"
    },
    {
      "cells": [
        "\"sriniiyer861@gmail.com\"",
        "\"Test!!233\""
      ],
      "line": 54,
      "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com;;2"
    },
    {
      "cells": [
        "\"sriniiyer862@gmail.com\"",
        "\"Test!!233\""
      ],
      "line": 55,
      "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 54,
  "name": "Use multiple data sets to signup on Zoho.com",
  "description": "",
  "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 19,
      "name": "@functional"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "Launch the browser \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "Open the Zoho application \"https://www.zoho.com\"",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "User clicks on Free Sign Up button \"//a[text()\u003d\u0027Free Sign Up\u0027]\"",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "User gets redirected to SignUpPage \"Create New Account - zoho.com\"",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "User enters data sets \"//*[@id\u003d\u0027emailfield\u0027]\" and \"sriniiyer861@gmail.com\" and \"//*[@id\u003d\u0027password\u0027]\" and \"Test!!233\" and \"//*[@id\u003d\u0027signup-termservice\u0027]\" in SignUp form",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 20
    }
  ],
  "location": "StepDefinitions.launch_browser(String)"
});
formatter.result({
  "duration": 11454444400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.zoho.com",
      "offset": 27
    }
  ],
  "location": "StepDefinitions.open_application(String)"
});
formatter.result({
  "duration": 3813791200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "//a[text()\u003d\u0027Free Sign Up\u0027]",
      "offset": 36
    }
  ],
  "location": "StepDefinitions.clicks_FreeSignUpbutton(String)"
});
formatter.result({
  "duration": 2005509600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create New Account - zoho.com",
      "offset": 36
    }
  ],
  "location": "StepDefinitions.redirect_SignUppage(String)"
});
formatter.result({
  "duration": 16187400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "//*[@id\u003d\u0027emailfield\u0027]",
      "offset": 23
    },
    {
      "val": "sriniiyer861@gmail.com",
      "offset": 51
    },
    {
      "val": "//*[@id\u003d\u0027password\u0027]",
      "offset": 80
    },
    {
      "val": "Test!!233",
      "offset": 106
    },
    {
      "val": "//*[@id\u003d\u0027signup-termservice\u0027]",
      "offset": 122
    }
  ],
  "location": "StepDefinitions.userenters_FormDetails_DataSets(String,String,String,String,String)"
});
formatter.result({
  "duration": 899413600,
  "status": "passed"
});
formatter.scenario({
  "line": 55,
  "name": "Use multiple data sets to signup on Zoho.com",
  "description": "",
  "id": "testing-zoho-application-home-page-features;use-multiple-data-sets-to-signup-on-zoho.com;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 19,
      "name": "@functional"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "Launch the browser \"chrome\"",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "Open the Zoho application \"https://www.zoho.com\"",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "User clicks on Free Sign Up button \"//a[text()\u003d\u0027Free Sign Up\u0027]\"",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "User gets redirected to SignUpPage \"Create New Account - zoho.com\"",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "User enters data sets \"//*[@id\u003d\u0027emailfield\u0027]\" and \"sriniiyer862@gmail.com\" and \"//*[@id\u003d\u0027password\u0027]\" and \"Test!!233\" and \"//*[@id\u003d\u0027signup-termservice\u0027]\" in SignUp form",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "chrome",
      "offset": 20
    }
  ],
  "location": "StepDefinitions.launch_browser(String)"
});
formatter.result({
  "duration": 10484128000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.zoho.com",
      "offset": 27
    }
  ],
  "location": "StepDefinitions.open_application(String)"
});
formatter.result({
  "duration": 4097002100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "//a[text()\u003d\u0027Free Sign Up\u0027]",
      "offset": 36
    }
  ],
  "location": "StepDefinitions.clicks_FreeSignUpbutton(String)"
});
formatter.result({
  "duration": 2561728600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create New Account - zoho.com",
      "offset": 36
    }
  ],
  "location": "StepDefinitions.redirect_SignUppage(String)"
});
formatter.result({
  "duration": 17274300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "//*[@id\u003d\u0027emailfield\u0027]",
      "offset": 23
    },
    {
      "val": "sriniiyer862@gmail.com",
      "offset": 51
    },
    {
      "val": "//*[@id\u003d\u0027password\u0027]",
      "offset": 80
    },
    {
      "val": "Test!!233",
      "offset": 106
    },
    {
      "val": "//*[@id\u003d\u0027signup-termservice\u0027]",
      "offset": 122
    }
  ],
  "location": "StepDefinitions.userenters_FormDetails_DataSets(String,String,String,String,String)"
});
formatter.result({
  "duration": 967236100,
  "status": "passed"
});
});