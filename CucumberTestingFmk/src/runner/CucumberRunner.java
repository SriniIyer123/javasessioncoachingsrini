package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="Features",glue="stepdefinitions",format= {"pretty","html:target/cucumber"})
public class CucumberRunner
{

}
