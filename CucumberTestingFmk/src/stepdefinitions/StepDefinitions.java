package stepdefinitions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import config.ConstantsFile;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.CommonUtils;

public class StepDefinitions {
CommonUtils obj=new CommonUtils();
//String: 	\"([^\"]*)\"   OR \"(.*)\"
	
// (\\d+)=>Integer
// (\\d+).(\\d+) Float or Double

//1st: hardcoding
//2nd: Data sets-> Scenario Outline=> common to entire testscript
//3rd: Data Table at a particular step- Given/Then/When
//4th way: From excel sheet:
//Call utility class method to get the 2d sheet data array
//using that, we can access the testdata and pass to our method

WebDriver driver=null;	
@Given("^Launch the browser \"([^\"]*)\"$")
public void launch_browser(String browser)//glue
{
	obj.initializeProperties(ConstantsFile.CONFIGFILEPath);
	driver=obj.setUp(browser);		
}
	
@Given("^Open the Zoho application \"([^\"]*)\"$")
public void open_application(String application)//glue
{
	//obj.initializeProperties(ConstantsFile.CONFIGFILEPath);
	driver=obj.getApplication(application);	
}


@When("^User clicks on Free Sign Up button \"([^\"]*)\"$")
public void clicks_FreeSignUpbutton(String locatorxpath)//glue
{
	//obj.initializeProperties(ConstantsFile.CONFIGFILEPath);
	By obj1=obj.getBy(locatorxpath, "xpath");
	WebElement element=obj.formElement(obj1);
	obj.clickElement(element);
}

@Then("^User gets redirected to SignUpPage \"(.*)\"$")
public void redirect_SignUppage(String expectedText)//glue
{
   obj.assertTitle(expectedText);
}

@Then("^User enters \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" in SignUp form$")
public void userenters_FormDetails(String useremailLocator,String useremailValue,String passwordLocator,String passwordValue,String acceptTermsLocator)//glue
{
  WebElement element1=obj.formElement(By.xpath(useremailLocator));
  WebElement element2=obj.formElement(By.xpath(passwordLocator));
  WebElement element3=obj.formElement(By.xpath(acceptTermsLocator));
  obj.sendKeys(element1, useremailValue);
  obj.sendKeys(element2, passwordValue);  
  obj.clickElement(element3);
}

@Then("^User enters data sets \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" in SignUp form$")
public void userenters_FormDetails_DataSets(String useremailLocator,String useremailValue,String passwordLocator,String passwordValue,String acceptTermsLocator)//glue
{
  WebElement element1=obj.formElement(By.xpath(useremailLocator));
  WebElement element2=obj.formElement(By.xpath(passwordLocator));
  WebElement element3=obj.formElement(By.xpath(acceptTermsLocator));
  obj.sendKeys(element1, useremailValue);
  obj.sendKeys(element2, passwordValue);  
  obj.clickElement(element3);
}




@Then("^User enters details in signup form$")
public void userenters_signupform(DataTable details)
{
	
	/*List<List<String>> credentials=details.raw();
	for(int i=0;i<credentials.size();i++)//i=>row count
	{		
	  WebElement element1=obj.formElement(By.xpath(credentials.get(i).get(0)));
	  WebElement element2=obj.formElement(By.xpath(credentials.get(i).get(2)));
	  WebElement element3=obj.formElement(By.xpath(credentials.get(i).get(4)));
	  obj.sendKeys(element1, credentials.get(i).get(1));
	  obj.sendKeys(element2, credentials.get(i).get(3));  
	  obj.clickElement(element3);	  
	}*/
  
  List<List<String>> userDetails=details.raw();
  String useremailLocator=userDetails.get(0).get(0);	
  String useremailValue=userDetails.get(0).get(1);	
  String passwordLocator=userDetails.get(0).get(2);
  String passwordValue=userDetails.get(0).get(3);
  String acceptTermsLocator=userDetails.get(0).get(4);  
  
  WebElement element1=obj.formElement(By.xpath(useremailLocator));
  WebElement element2=obj.formElement(By.xpath(passwordLocator));
  WebElement element3=obj.formElement(By.xpath(acceptTermsLocator));
  obj.sendKeys(element1, useremailValue);
  obj.sendKeys(element2, passwordValue);  
  obj.clickElement(element3);
}

@Then("User quits the browser$")
public void quitBrowser()
{
	obj.quitBrowser();
}
}
