package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import base.BaseClass;

public class ExcelHelper extends BaseClass {
	public static Object[][] ExcelTestData;//ExcelHelper.ExcelTestData
	int rowCount,colCount=0;
	//Workbook Sheet Row Cell
	//XSSFWorkbook =>.xlsx and HSSFWorkbook=>.xls//Testcases
	Workbook workbook;
	Sheet sheet;
	Row row;
	Cell cell;
	//CommonMethods common=new CommonMethods();
	
	public static Object getCellData(int row, int col)
	{
		return ExcelTestData[row][col];		
	}
	
	public void printExcelData()
	{
		System.out.println("ExcelValues:");
		for(int i=0;i<ExcelTestData.length;i++)//row number is starting from 1
		{
			for(int j=0;j<ExcelTestData[0].length;j++)//cols starting from 0
			{
				System.out.print(ExcelTestData[i][j]+" ");
			}
			System.out.println();
		}		
		
	}
	
	public void readfromExcel(String RelFilepath,String sheetname)
	{	
		FileInputStream fip=null;
		File file=null;
		Object val=null;
		
		try {
		String filepath=System.getProperty("user.dir")+RelFilepath;// .xlsx
		String filepathobjs[]=filepath.split("/");
		String ExcelFileName=filepathobjs[filepathobjs.length-1];
		System.out.println("ExcelFileName:"+ExcelFileName);
		
		file=new File(filepath);		
		fip=new FileInputStream(file);
		String fileext=ExcelFileName.substring(ExcelFileName.indexOf('.')+1);
		System.out.println("FileExtension:"+fileext);
		
		if(fileext.equals("xlsx"))
		{
			workbook=new XSSFWorkbook(fip);	
			sheet=workbook.getSheet(sheetname);
			rowCount=(sheet.getLastRowNum()-sheet.getFirstRowNum());
			row=sheet.getRow(3);
			colCount=row.getLastCellNum();
			System.out.println("Row:"+rowCount+" Col:"+colCount);
			ExcelTestData=new Object[rowCount][colCount];
			
			for(int i=0;i<rowCount;i++)//rows 1->0
			{
				row=sheet.getRow(i+1);//1st row onwards data is stored	 //i->i+1			
				for(int j=0;j<colCount;j++)
				{
										
					cell=row.getCell(j);
					if(Cell.CELL_TYPE_NUMERIC==cell.getCellType())
					{
						val=cell.getNumericCellValue();
						ExcelTestData[i][j]=val;
					}
					else if(Cell.CELL_TYPE_BOOLEAN==cell.getCellType())
					{
						val=cell.getBooleanCellValue();
						ExcelTestData[i][j]=val;
					}
					else if(Cell.CELL_TYPE_STRING==cell.getCellType())
					{
						val=cell.getStringCellValue();
						ExcelTestData[i][j]=val;
					}	
								
				
				}//cols
				
				  

				
			}//row will end 
			
			
		}
		else if(fileext.equals("xls"))
		{
			workbook=new HSSFWorkbook(fip);	
			sheet=workbook.getSheet("Testcases");			
		}		
		
		}catch(FileNotFoundException e) {
			System.out.println("FileNotFoundException caught in readfromExcel:"+e.getMessage());
		}
		catch(Exception e) {
			System.out.println("Exception caught in readfromExcel:"+e.getMessage());

		}
		finally {
			try {
				fip.close();
			} catch (IOException e) {
				System.out.println("IOException caught in readfromExcelmethod finally clause");
			}
			//file.
		}
	}

/*	
	//Used for realtime excel interaction to work with our parameters in excel sheet, without dataprovider
	public void runtimeExcelInteractor(String RelFilepath)
	{	
         Object val=null;
			for(int i=0;i<ExcelTestData.length;i++) {
			    val=getCellData(i,2);//3=>property:objLocator
				By obj=common.getBy((String)val);//call method to form webelement
				WebElement element=common.formElement(obj);
				String value=(String)getCellData(i,3);
				common.sendKeys(element, value);//3:property:SearchTextValue
			}
	}
*/
	
	//Colnum: int
	//Colnum:String
	public void writetoExcel(String sheetname,int rowNum,int colNum,Object objVal)//5th ind:PASS/FAIL
	{		
		FileOutputStream fop;
		File file;
		FileInputStream fip;
		try {
		String filepath=System.getProperty("user.dir")+"//resources//Testdemo.xlsx";// .xlsx
		String filepathobjs[]=filepath.split("/");
		String ExcelFileName=filepathobjs[filepathobjs.length-1];
		System.out.println("ExcelFileName:"+ExcelFileName);
		boolean Stringcont=false;
		boolean NumCont=false;
		boolean BooleanCont=false;
		
		file=new File(filepath);		
		fip=new FileInputStream(file);
		String fileext=ExcelFileName.substring(ExcelFileName.indexOf('.')+1);
		System.out.println("FileExtension:"+fileext);
		
		if(fileext.equals("xlsx"))
		{
			workbook=new XSSFWorkbook(fip);	
			sheet=workbook.getSheet("Testcases");
			rowCount=(sheet.getLastRowNum()-sheet.getFirstRowNum())+1;//7
			row=sheet.getRow(0);
			colCount=row.getLastCellNum();			
			
			row=sheet.createRow(rowCount);//at 7th index since this is the next row after 6th index			
      		for(int j=0;j<colCount;j++)
			{
				cell=row.createCell(j);
				//cell.setCellVa
				cell.setCellValue((String)objVal);
			}				
					
			row=sheet.getRow(rowNum);
			cell=row.getCell(colNum);				
				
			if(Cell.CELL_TYPE_STRING==cell.getCellType())
			{
				cell.setCellValue((String)objVal);
				Stringcont=true;				
			}
			else if(Cell.CELL_TYPE_BOOLEAN==cell.getCellType())
			{
				cell.setCellValue((boolean)objVal);
				BooleanCont=true;
			}
			else if(Cell.CELL_TYPE_NUMERIC==cell.getCellType())
			{
				cell.setCellValue((double)objVal);
				NumCont=true;
			}
			if(Stringcont)
			 System.out.println("Check if cell data is updated:"+cell.getStringCellValue());
			else if(BooleanCont)
			 System.out.println("Check if cell data is updated:"+cell.getBooleanCellValue());
			else if(NumCont)
			 System.out.println("Check if cell data is updated:"+cell.getNumericCellValue());
			
			fop=new FileOutputStream(file);
			workbook.write(fop);			
			fip.close();
			fop.close();
			//file.delete();			
		}		
		}catch(FileNotFoundException e) {
			System.out.println("FileNotFoundException caught in readfromExcel:"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("IOException caught in readfromExcel:"+e.getMessage());
		}
		catch(Exception e) {
			System.out.println("Exception caught in readfromExcel:"+e.getMessage());
		}
		finally {
			System.out.println("Finally clause");
		}
			
	}

	//Method Overloading
	public void writetoExcel(String SheetName,int rowNum,HashMap<Integer,Object> writeData)//5th ind:PASS/FAIL
	{		
		FileOutputStream fop=null;
		File file=null;
		FileInputStream fip=null;
		try {
		String filepath=System.getProperty("user.dir")+"//resources//Testdemo.xlsx";// .xlsx
		String filepathobjs[]=filepath.split("/");
		String ExcelFileName=filepathobjs[filepathobjs.length-1];
		System.out.println("ExcelFileName:"+ExcelFileName);
		boolean Stringcont=false;
		boolean NumCont=false;
		boolean BooleanCont=false;
		
		file=new File(filepath);		
		fip=new FileInputStream(file);
		String fileext=ExcelFileName.substring(ExcelFileName.indexOf('.')+1);
		//System.out.println("FileExtension:"+fileext);
		
		if(fileext.equals("xlsx"))
		{
			workbook=new XSSFWorkbook(fip);	
			sheet=workbook.getSheet(SheetName);
			row=sheet.getRow(rowNum);
            
			Set<Entry<Integer,Object>> setEntries=writeData.entrySet();
			Iterator<Entry<Integer,Object>> Itrentries=setEntries.iterator();
			while(Itrentries.hasNext())
			{
				Entry<Integer,Object> entry=Itrentries.next();
				int colNum=entry.getKey();
				Object value=entry.getValue();			
				cell=row.getCell(colNum);
				if(Cell.CELL_TYPE_STRING==cell.getCellType())
				{
					cell.setCellValue((String)value);								
				}
				else if(Cell.CELL_TYPE_BOOLEAN==cell.getCellType())
				{
					cell.setCellValue((boolean)value);					
				}
				else if(Cell.CELL_TYPE_NUMERIC==cell.getCellType())
				{
					cell.setCellValue((double)value);					
				}
				
			}//while loop ends for iteration of HashMap
						
			fop=new FileOutputStream(file);
			workbook.write(fop);			
			
			//file.delete();			
		}		
		}catch(FileNotFoundException e) {
			System.out.println("FileNotFoundException caught in readfromExcel:"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("IOException caught in readfromExcel:"+e.getMessage());
		}
		catch(Exception e) {
			System.out.println("Exception caught in readfromExcel:"+e.getMessage());
		}
		finally {
			//System.out.println("Finally clause");
			try {
				fip.close();
				fop.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			
	}
	
	public ExcelHelper() {
		// TODO Auto-generated constructor stub
	}

	/*public static void main(String[] args) {
		ExcelHelper excelT=new ExcelHelper();
		excelT.readfromExcel();
		excelT.printExcelData();
		System.out.print("Cell Data:"+excelT.getCellData(6,3));//row num=6 col num=4
		excelT.writetoExcel(3, 5, "PASS");//rownum starts from 0 so rownum=2=> 3 and colnum:5 => 6
		excelT.readfromExcel();
		excelT.printExcelData();
	}*/

}
