package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class CommonUtils {
WebDriver driver=null;
Properties prop=null;
FileInputStream fip=null;
File file=null;

public void initializeProperties(String FilePath)
{
	try {
		   //FilePath=FilePath.toUpperCase();
			file=new File(FilePath);//ConstantsFile.ConfigPropertiesPath);		
			fip=new FileInputStream(file);//FileReader fileread
		    prop=new Properties();
			prop.load(fip);//Loads the inputstream by properties file object			
	} catch (FileNotFoundException e) {
		System.out.println("FileNotFoundException caught in initializeProperties:"+e.getMessage());
	} 
	catch(Exception e)
	{
		System.out.println("Exception caught in initializeProperties:"+e.getMessage());
	}		
}

//above method to be called first
public String readProperty(String propertyName)
{
	
   return prop.getProperty(propertyName);

}

public By getBy(String locator, String type)
{
	By obj=null;
	
	//xpath, id, className, cssSelector,linkText,name,tagName,partialLinkText
    switch(type)
   {
    case "xpath":
    	  obj=By.xpath(locator);        	
         break;
    
    case "id":
    	  obj=By.id(locator);
    	 break;
    
    case "cssSelector":
    	obj=By.cssSelector(locator);
    	 break;
    	 
    case "name":
    	obj=By.name(locator);
    	
    	 break;
    	 
    case "tagName":
    	obj=By.tagName(locator);
    	 break;
    	 
    case "linkText":
    	obj=By.linkText(locator);
    	 break;
    	 
    case "partialLinkText":
    	obj=By.partialLinkText(locator);
    	break;       	
         
    case "className":
    	obj=By.className(locator);
    	break;      
   
   }		
	//readProperty(ORProperties,propertyname);
	return obj;		
}

public WebElement formElement(By byValue)
{
	return driver.findElement(byValue);
}

public String getPageTitle()
{
	return driver.getTitle();
}

public void clickElement(WebElement element)
{
	element.click();
}

public void sendKeys(WebElement element, String sendkeys)
{
	element.sendKeys(sendkeys);
}

public boolean isElementDisplayed(WebElement element)
{
	return element.isDisplayed();
}

public void quitBrowser()
{
	driver.quit();
}


	public WebDriver setUp(String browser) {
		String ChromeDriverP=readProperty("ChromeDriverPath");
		
		switch(browser)
		{
		
		case "chrome":
			System.setProperty("webdriver.chrome.driver",readProperty("ChromeDriverPath"));
			driver=new ChromeDriver();
			
		break;
		
		case "ie":
			
		break;
		
		case "firefox":
			
		break;
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		return driver;
	}
	
	public WebDriver getApplication(String application)
	{
		driver.get(application);
		return driver;
	}
	
	public void assertTitle(String Value)
    {
       try
       {      
         Assert.assertEquals(getPageTitle(), Value);
       }
       catch(Exception e)
       {
    	 System.out.println("Exception caught in getPageTitle: "+e.getMessage());
    
       }
       catch(AssertionError assertErr)
       {
       	   System.out.println("AssertionError caught in getPageTitle: "+assertErr.getMessage());
    	   
       }
    
    }

}
