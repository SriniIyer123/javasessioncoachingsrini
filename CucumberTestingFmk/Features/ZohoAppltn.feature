#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@functional
Feature:   Testing Zoho Application Home Page Features
  I want to use this template for my Zoho application

@functional
Scenario:  Perform Positive scenario of Sign Up on Zoho Application
#glue=>StepDefinition
Given Launch the browser "chrome"
And  Open the Zoho application "https://www.zoho.com"
When User clicks on Free Sign Up button "//a[text()='Free Sign Up']"
Then User gets redirected to SignUpPage "Create New Account - zoho.com"
And User enters "//*[@id='emailfield']" and "sriniiyer861@gmail.com" and "//*[@id='password']" and "Test1234" and "//*[@id='signup-termservice']" in SignUp form
And User quits the browser

#User enters \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" in SignUp form

#glue=>StepDefinition
@functional
Scenario:  Perform Positive scenario of Sign Up on Zoho Application with Data Table
Given Launch the browser "chrome"
And  Open the Zoho application "https://www.zoho.com"
When User clicks on Free Sign Up button "//a[text()='Free Sign Up']"
Then User gets redirected to SignUpPage "Create New Account - zoho.com"
And User enters details in signup form
      | //*[@id='emailfield'] | sriniiyer861@gmail.com | //*[@id='password'] | Test1234 | //*[@id='signup-termservice'] |
      | //*[@id='emailfield'] | sriniiyer862@gmail.com | //*[@id='password'] | Test1234 | //*[@id='signup-termservice'] |
      | //*[@id='emailfield'] | sriniiyer863@gmail.com | //*[@id='password'] | Test1234 | //*[@id='signup-termservice'] |
      | //*[@id='emailfield'] | sriniiyer864@gmail.com | //*[@id='password'] | Test1234 | //*[@id='signup-termservice'] |
And User quits the browser

@functional
  Scenario Outline: Use multiple data sets to signup on Zoho.com
  Given Launch the browser "chrome"
  And  Open the Zoho application "https://www.zoho.com"
  When User clicks on Free Sign Up button "//a[text()='Free Sign Up']"
  Then User gets redirected to SignUpPage "Create New Account - zoho.com"
  And User enters data sets "//*[@id='emailfield']" and <emailValue> and "//*[@id='password']" and <passwordValue> and "//*[@id='signup-termservice']" in SignUp form
  Examples: 
      | emailValue                  | passwordValue |
      | "sriniiyer861@gmail.com"     | "Test!!233"  |
      | "sriniiyer862@gmail.com"     | "Test!!233"  |
