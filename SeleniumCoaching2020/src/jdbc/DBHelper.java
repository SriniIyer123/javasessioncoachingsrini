package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//Log4j
//Logging=> logs enable u to know wht went wrong and where(line number of code, method name)
public class DBHelper {

//query2-> for DML, query1=>DDL
	public static void connectDB(String DDLquery1, String DMLquery2)
	{
		//UserName, password, Host/IP, Port Number, Database
		//Connection String: jdbc:mysql://HostIP:PortNumber/dbname
		String url="jdbc:mysql://localhost:3306/seleniumpractice";//connection string
		String user="root";
		String password="Test@123";
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		
		try {
		//Driver which will allow us to connect Oracle mysql
		//Step1: Use/Load appropriate driver for the respective DB
		Class.forName("com.mysql.jdbc.Driver");//oracle.jdbc.driver.OracleDriver=>Oracle
		//Step2, we want to create a Connection to database using getConnection method
		conn=DriverManager.getConnection(url, user, password);
		//Step3, we want to create a statement which can be executed in connected database
		stmt=conn.createStatement();
		//Step4: Execute query using the statement object
		
		int a=stmt.executeUpdate(DMLquery2);
		System.out.println("Affected Rows due to DML Update query:"+a);
	
		System.out.println("Data after doing update in entire table");
		rs=stmt.executeQuery(DDLquery1);//select  executeUpdate(sql)=> for DML or updating tables
		//Step5: Iterate over the results and print the output
		System.out.println("Getting query output:");
		while(rs.next())//works unlike an array and index starts from 1
		{
			System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4));
		}
		
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFoundException exception caught in connectDB:"+e.getMessage());
		}
		catch(SQLException sqle)
		{
			System.out.println("SQLException exception caught in connectDB:"+sqle.getMessage());
		}
		catch(Exception e)
		{
			System.out.println("Exception exception caught in connectDB:"+e.getMessage());
		}	
		
		
		
	}

	public static void main(String[] args) {
		DBHelper.connectDB("select * from empdetails","update empdetails set address='Pune' where empid='2124'");

	}

}
