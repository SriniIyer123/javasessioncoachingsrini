package testng;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import config.ConstantsFile;

public class TestNGBasics{
public WebDriver driver;
ExtentReports extRept;
ExtentTest extTest;

//Test Annotation=>This is a test case method signUp(Registration) 
//Submit=> one user data entry would be created and user will get confirmation mail.
//Test Class  Method  Suite 	
	
/*public void clickElement(WebElement element)
{
	element.click();
}
Suite => Test => Class => Method
*/

/*  testng.TestNGBasics- Seq of steps:
b4Suite 0 ms
b4Test 18 ms
b4Class 26 ms
b4method 31 ms
signUp 31 ms
afterMethod 50 ms
b4method 54 ms
quitBrowser 54 ms
afterMethod 59 ms
afterTest 60 ms
afterSuite
 * 
 * 
 * 
 */

public static By formLocator(String Locatorname)
{//Returns the locator
	String value="";
	value=ConstantsFile.Google_Search_TextBox_xpath;
	//value=value.toLowerCase();
	
	By element=null;
	int length=0;
	String[] str= Locatorname.split("_");
	length=str.length;
	
	System.out.println("Length of str String array:"+str.length);//0 to 3
	String lastVal=str[length-1];
	 
	switch(lastVal)
	{
	case "id":
	element=By.id(value);
	
	break;
	
	case "cssselector":
		element=By.cssSelector(value);
		
	break;
	
	case "xpath":
		element=By.xpath(value);
		
	break;	
	
	case "name":
		element=By.name(value);
		
	break;
	
	case "classname":
		element=By.className(value);
	break;	
	
	
	}
	return element;
}


@Test(enabled=false,priority=1)
@Parameters({"username","SearchCriteria"})
public void searchGoogle(@Optional("default user")String username, @Optional("Selenium")String SearchCriteria)
{
	By TextBoxelement=null;
	By SearchButtonelement=null;
	System.out.println("User:"+username +" has logged in");
	TextBoxelement=formLocator("Google_Search_TextBox_xpath");
	//SearchButtonelement=formLocator("Google_Search_SearchButton_name");
	driver.findElement(TextBoxelement).sendKeys(SearchCriteria);//entering the value in searchtextbox
	driver.findElement(By.name("btnK")).click();
	System.out.println("Page Title:"+driver.getTitle());	
}

@Test(enabled=false,priority=2,dataProvider="provideData",description="UseDataProviderTC")
public void performdataproviderTesting(Object o1, Object o2)
{
	By TextBoxelement=null;
	By SearchButtonelement=null;
	System.out.println("User:"+o1 +" has logged in");
	TextBoxelement=formLocator("Google_Search_TextBox_xpath");
	//SearchButtonelement=formLocator("Google_Search_SearchButton_name");
	driver.findElement(TextBoxelement).sendKeys((String)o2);//entering the value in searchtextbox
	driver.findElement(By.name("btnK")).click();
	System.out.println("Page Title:"+driver.getTitle());	
}


@DataProvider(name="provideData")//will be changed implementation , once we read data from excel
public Object[][] providedatatoTestcases()
{
	Object[][] objarr={ {"Srini","Selenium"},
			   {"Deepak","Python"},
			   {"Viji","Java"},
			   {"Ajay","API Automation"},
			   {"Vijay","RPA"}
		   	};
	
	/*for(int i=0;i<objarr.length;i++)//no of rows: 0 to 4
	{
		for(int j=0;j<objarr[0].length;j++)//no of cols 0 to 4
		{
			objarr[i][j]="Selenium"+i+j;			
		}
	}*/
	return objarr;	
}


@Test(enabled=false,priority=0,groups={"smoke","functional","sanity","regression"},description="This deals with signing up on an website")//Test Case Method =>Test Method(Method)
public void signUp()//testdata could be in some other method or browser has to be launched
{//on google application
	System.out.println("In signUp Test Method");
	//throw new ArithmeticException();
	/*try {
	//throw new ArithmeticException();	
    Reporter.log("In signUpmethod");
	Assert.assertTrue(2>3);//Assertion Pass	
	}
	catch(AssertionError assertErr)
	{
		Reporter.log("AssertionError caught in signUp method:"+assertErr.getMessage());
		System.out.println("AssertionError caught in signUp method:"+assertErr.getMessage());
	}
	catch(Error err)
	{
		Reporter.log("Error caught in signUp method:"+err.getMessage());
		System.out.println("Error caught in signUp method:"+err.getMessage());
	}
	catch(Exception e)//checked exception or  uncheckedexception/logical or run-time
	{
		Reporter.log("Exception caught in signUp method:"+e.getMessage());
		System.out.println("Exception caught in signUp method:"+e.getMessage());
	}
	finally {
		Reporter.log("In finally clause");
		System.out.println("Will i be executed in signUp?");
	}*/
}

/*
 * //WebElement firstname=driver.findElement(By.xpah("xpath"));
	//clickElement(firstname);
	//other class method of some class or other pkg
 */
@Test(enabled=true,priority=1,groups={"smoke","functional","sanity","regression"},description="This deals with quitting from an application")//,dependsOnMethods={"signUp"})
public void quitBrowser()//on google application
{
	Reporter.log("InquitBrowser method");
	extTest.log(LogStatus.INFO,"INFO Message");
	System.out.println("Description:"+extTest.getDescription());
	extTest.addScreenCapture("C:\\Users\\Srinivasan\\Desktop\\screen1.png");
	extTest.log(LogStatus.PASS, "Quitting browser", extTest.addScreenCapture("C:\\Users\\Srinivasan\\Desktop\\screen1.png"));
	System.out.println("In quitBrowser");
}

@BeforeMethod(groups={"smoke","functional","sanity","regression"})//Before a Test method(Method) is executed , this method gets executed.
public void b4method()
{
	try {
	//Launching the Google Application in launched Chrome browser
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//resources//chromedriver.exe");
	extTest.log(LogStatus.INFO, "INFO:in b4method");
	driver=new ChromeDriver();
	driver.get(ConstantsFile.applicationUrl);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	Reporter.log("In B4Method");
	System.out.println("In B4Method");
	extTest.log(LogStatus.PASS, "PASS:in b4method");
	}
	catch(Exception e)
	{
		System.out.println("Exception caught in b4method:"+e.getMessage());
		extTest.log(LogStatus.FAIL, "FAIL:Validation failed"+e.getMessage());
	}
	
	
}

@AfterMethod(groups={"smoke","functional","sanity","regression"})//After a Test method(Method) is executed , this method gets executed.
public void afterMethod()
{
	Reporter.log("In AfterMethod");
	System.out.println("In AfterMethod");
	driver.quit();
}

@BeforeClass //Class
public void b4Class()
{
	Reporter.log("In B4Class");
	System.out.println("In B4Class");
}

@AfterClass //Class
public void afterClass()
{
	Reporter.log("In afterClass");
	System.out.println("In afterClass");
}


@BeforeTest
public void b4Test()
{
	//Launching the browser
	Reporter.log("In B4Test");
	System.out.println("In B4Test");
	extTest=extRept.startTest("Test Case1");
}

@AfterTest
public void afterTest()
{
	Reporter.log("In AfterTest");
	System.out.println("In AfterTest");
	extRept.endTest(extTest);
}

@BeforeSuite
public void b4Suite()
{
	extRept=new ExtentReports(System.getProperty("user.dir")+"//ExecutionReport//ExtentReport.html");	
	//how to add column step description in extent report.
	Reporter.log("In B4Suite");
	//Establishing connection
	System.out.println("In B4Suite");
}

@AfterSuite
public void afterSuite()
{
	Reporter.log("In afterSuite");
	System.out.println("In afterSuite");
	extRept.flush();
}


@DataProvider
public void provideData1()
{
	
}

@Parameters
public void useParam()
{
	
}
}
