package testng;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNGLogs {
static WebDriver driver;
Logger log=null;


    static void setUp(String browser)//Chrome chrome CHROME
	{
    	browser=browser.toLowerCase();
    	switch(browser)
    	{
    	case "chrome":
    		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
    		driver=new ChromeDriver();    		    	
    		break;
    		
    	case "firefox":	
    	   
    	case "ie":
    		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "//resources//chromedriver.exe");
    		driver=new InternetExplorerDriver();
    		break;
    	
    	default:
    	      System.out.println("Incorrect browsername");
    	      break;
    	}
    	System.out.println("BrowserName:"+browser);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
    	//driver.get(application);    	
	}

    @BeforeTest
    public void b4Test()
    {
    	log=Logger.getLogger("qaLogs"); //.getRootLogger();
    	PropertyConfigurator.configure(System.getProperty("user.dir")+"//src//logs//log4j.properties");  	   	
    }
    
    
    
	@BeforeMethod
	public void b4meth()
	{
		TestNGLogs.setUp("chrome");//,"https://www.gmail.com");
		System.out.println("In b4meth");
		log.info("In Before Method");
	}

	@AfterMethod
	public void aftermeth()
	{
		driver.quit();
		System.out.println("In aftermeth");
		log.info("In After Method");
	}
	
	@Test
	public void test1()
	{
		try {
		driver.get("https://www.gmail.com");
		System.out.println("Page Title:"+driver.getTitle());
		log.info("Page Title of test1:"+driver.getTitle());
		Assert.assertEquals(false,true);
		}
		catch(Exception e)
		{
			System.out.println("Exception caught in test1 method"+e.getMessage());
			log.error("Exception caught in test1 method"+e.getMessage());
		}
		catch(AssertionError asert)
		{
			System.out.println("Exception caught in test1 method"+asert.getMessage());
			log.error("AssertionError caught in test1 method"+asert.getMessage());
		}
	}

	@Test
	public void test2()
	{
		driver.get("https://www.facebook.com");
		log.info("Page Title of test2:"+driver.getTitle());
		System.out.println("Page Title:"+driver.getTitle());
	}
	
	
	@Test
	public void test3()
	{
		driver.get("https://www.google.com");
		System.out.println("Page Title:"+driver.getTitle());
		log.info("Page Title of test3:"+driver.getTitle());
	}

	@Test
	public void test4()
	{
		driver.get("https://www.yahoo.com");
		System.out.println("Page Title:"+driver.getTitle());
		log.info("Page Title of test4:"+driver.getTitle());
	}
}

