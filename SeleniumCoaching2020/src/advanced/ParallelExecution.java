package advanced;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ParallelExecution {

	private static ParallelExecution instance=null;
	
	ThreadLocal<WebDriver> driver=new ThreadLocal<WebDriver>();
	
	//Constructor as private
	private ParallelExecution() {
		
	}
	
	public static ParallelExecution getInstance()
	{
		if(instance==null)
		{
			instance=new ParallelExecution();
		}
		
		return instance;
	}

	public final void setDriver(String type)
	{
		
		switch(type)
		{
		case "chrome":
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
     		driver.set(new ChromeDriver());
     		driver.get().manage().window().maximize();
     		driver.get().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
     		driver.get().manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
     		break;

		
		case "ie":
			
		break;
		
		default:
			
		break;
		
		}
		
	}
	
	public WebDriver getDriver()
	{
		return driver.get();
	}
	


}
