package advanced;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class OtherMouseActions {

	static WebDriver driver;
	Actions actions;
	Action action;
	 //setting up browser
	 	
	     static void setUp(String browser,String application)//Chrome chrome CHROME
	 	{
	     	browser=browser.toLowerCase();
	     	switch(browser)
	     	{
	     	case "chrome":
	     		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//Drivers//chromedriver.exe");
	     		driver=new ChromeDriver();    		    	
	     		break;
	     		
	     	case "firefox":	
	     	   
	     	case "ie":
	     		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "//Drivers//chromedriver.exe");
	     		driver=new InternetExplorerDriver();
	     		break;
	     	
	     	default:
	     	      System.out.println("Incorrect browsername");
	     	      break;
	     	}
	     	System.out.println("BrowserName:"+browser);
	 		driver.manage().window().maximize();
	 		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	     	driver.get(application);    	
	 	}
	
	     //Reusable utility method with sending text on the element where cursor is moved
	     public void movetoElement(String xpath, String texttoSend)
	     {
	    	WebElement element=driver.findElement(By.xpath(xpath));
	    	actions=new Actions(driver);
	    	actions.moveToElement(element).click().sendKeys(texttoSend).build().perform();
	    	//action.perform();
	    	actions.contextClick(element).perform();
	    		    	
	     }

	     //Reusable utility method for moving cursor to element specified
	     public void movetoElement(String xpath)
	     {
	    	WebElement element=driver.findElement(By.xpath(xpath));
	    	actions=new Actions(driver);
	    	actions.moveToElement(element).click().perform();	    	 
	     }
	     
	     public void useKeyboardKeys()
	     {
	    	 
	     }
	     
	     //Test Case Method
	     public void signUpGmail()
	     {
	    	 System.out.println("Page Title:"+driver.getTitle());
	    	 //movetoElement("//span[contains(text(),'Create account')]");	//Its correct
	    	 //movetoElement("//*[contains(text(),'For myself')]");
	    	 movetoElement("//*[@id='user-message']","Test");
	     }
	     
	public static void main(String[] args) {
		OtherMouseActions obj=new OtherMouseActions();
		setUp("chrome","https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        obj.signUpGmail(); 
	}

}
