package advanced;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Tests {

	public WebDriver driver=null;
	
	@BeforeClass
	public void b4Cl()
	{
		//driver=
		
	}
	
	@Test
	public void performTest1()
	{
		ParallelExecution obj=ParallelExecution.getInstance();
		obj.setDriver("chrome");
		obj.getDriver().get("https://www.google.com");
		System.out.println("Page Title"+obj.getDriver().getTitle());		
	}

	@Test(enabled=false)
	public void performTest2()
	{
		ParallelExecution obj1=ParallelExecution.getInstance();
		obj1.setDriver("chrome");
		obj1.getDriver().get("https://www.gmail.com");
		System.out.println("Page Title"+obj1.getDriver().getTitle());		
	}
}
