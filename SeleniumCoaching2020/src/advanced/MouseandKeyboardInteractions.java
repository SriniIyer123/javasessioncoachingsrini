package advanced;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.remote.RemoteKeyboard;
import org.openqa.selenium.support.events.internal.EventFiringKeyboard;
import org.openqa.selenium.support.events.internal.EventFiringMouse;

/* Drag and drop: http://demo.guru99.com/test/drag_drop.html
 * 
 * 
 */



public class MouseandKeyboardInteractions {

	   static WebDriver driver;
		 //setting up browser
		 	
		     static void setUp(String browser,String application)//Chrome chrome CHROME
		 	{
		     	browser=browser.toLowerCase();
		     	switch(browser)
		     	{
		     	case "chrome":
		     		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
		     		driver=new ChromeDriver();    		    	
		     		break;
		     		
		     	case "firefox":	
		     	   
		     	case "ie":
		     		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "//resources//chromedriver.exe");
		     		driver=new InternetExplorerDriver();
		     		break;
		     	
		     	default:
		     	      System.out.println("Incorrect browsername");
		     	      break;
		     	}
		     	System.out.println("BrowserName:"+browser);
		 		driver.manage().window().maximize();
		 		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
		     	driver.get(application);    	
		 	}
		
		     public static void dragandDrop(WebElement source,WebElement target)
		     {
		    	 Keyboard key=new EventFiringKeyboard(driver, null);//2ndparam-webdriver event listener
		    	 Mouse mouse=new EventFiringMouse(driver, null);//2ndparam-webdriver event listener
		    	 		    	 
		    	 //Step1: Create an object of Actions class
		    	 Actions actions=new Actions(driver);
		    	// Actions actions1=new Actions(key);
		    	// Actions actions2=new Actions(key,mouse);
		    	 
		    	 //Step2: Drag and Drop
		    	 actions.dragAndDrop(source, target).perform();//since no multiple actions involved directly use perform() rather than build()
		     }
		     
		     public static void movetoElement(WebElement element)
		     {
		    	 Actions actions=new Actions(driver);
		    	 actions.moveToElement(element).perform();
		    	 //Action action= actions.moveToElement(element).click(element).build();//whenever more thn 1 event then need to use build() method
		         //action.perform();//perform the compiled action OR Execute it.
		     }

	public static void main(String[] args) {
	
		setUp("chrome","http://demo.guru99.com/test/drag_drop.html");
		
  		driver.switchTo().frame(driver.findElement(By.id("flow_close_btn_iframe")));//Close frame	    
	    driver.findElement(By.id("closeBtn")).click();
	    
	    driver.switchTo().defaultContent();
	    
		WebElement source_BANK=driver.findElement(By.xpath("//*[@id='credit2']/a"));
		//movetoElement(source_BANK);		
		WebElement target_ACCOUNT=driver.findElement(By.xpath("//*[@id='bank']/li"));
		dragandDrop(source_BANK,target_ACCOUNT);		
	}
/*		keyDown(Keys.ALT);//simulating pressing of ALT button in keyboard
		driver.findElement(By.xpath(""));
		driver.findElement(By.xpath(""));
		keyUp(Keys.ALT);//simulating release of the pressed ALT button
		
		Keys.CONTROL//automate whatever user had to select options from keyboard- in a script
*/
	
	

}
