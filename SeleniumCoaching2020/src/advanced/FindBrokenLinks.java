package advanced;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class FindBrokenLinks {

	static WebDriver driver;
	static HashMap<Integer,String> BrokenLinkCodeMesg=new HashMap<Integer,String>();
	
	     static void setUp(String browser,String application)//Chrome chrome CHROME
	 	{
	     	browser=browser.toLowerCase();
	     	switch(browser)
	     	{
	     	case "chrome":
	     		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
	     		driver=new ChromeDriver();    		    	
	     		break;
	     		
	     	case "firefox":	
	     	   
	     	case "ie":
	     		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "//resources//chromedriver.exe");
	     		driver=new InternetExplorerDriver();
	     		break;
	     	
	     	default:
	     	      System.out.println("Incorrect browsername");
	     	      break;
	     	}
	     	System.out.println("BrowserName:"+browser);
	 		driver.manage().window().maximize();
	 		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	     	driver.get(application);    	
	 	}

	public static void getBrokenLinks() {
		//By.tagName("a")
		HttpURLConnection huc = null;
		List<WebElement> elements=driver.findElements(By.tagName("a"));
		Iterator<WebElement>Itrelements= elements.iterator();
			
		while(Itrelements.hasNext())
		{
			WebElement element=Itrelements.next();
			String url=element.getAttribute("href");
			System.out.println("href url:"+url);
			try {
				huc=(HttpURLConnection)(new URL(url).openConnection());
				huc.setRequestMethod("HEAD");
				huc.connect();
				int respCode=huc.getResponseCode();
				String RespMessage=huc.getResponseMessage();
				
				if(respCode>=400)
					BrokenLinkCodeMesg.put(respCode, RespMessage);
				
			} 
			catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
			catch (IOException e) {
				System.out.println("IOException caught in method:getBrokenLinks"+e.getMessage());
			}
		}
		
		
	}

	public static void printBrokenLinkDetails()
	{
		Set<Map.Entry<Integer,String>> setLinks=BrokenLinkCodeMesg.entrySet();
		Iterator<Map.Entry<Integer,String>> ItrLinks=setLinks.iterator();
		
		while(ItrLinks.hasNext())
		{
			Entry<Integer,String> entry=ItrLinks.next();
			int key=entry.getKey();
			String val=entry.getValue();
		    System.out.println("RespCode:"+key+" ResponseErrorMessg:"+val);
		    System.out.println();
		}
		
	}
	
	public static void main(String[] args) {
		setUp("chrome","http://www.zlti.com");
		getBrokenLinks();
		printBrokenLinkDetails();

	}

}
