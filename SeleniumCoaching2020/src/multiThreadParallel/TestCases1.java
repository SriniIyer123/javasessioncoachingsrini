package multiThreadParallel;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestCases1 {
WebDriver driver;

@Parameters({"browser","applnurl"})	
@Test
public void Test1(String browserName,String applnurl)
{
		ParallelExecution obj=ParallelExecution.getInstance();
		obj.SetDriver(browserName);
		driver=obj.getDriver();
		driver.get(applnurl);
		System.out.println("Page Title:"+driver.getTitle());
		System.out.println(Thread.currentThread().getId());//if namme is different or id is different=>Then multithreading or parallel execution is done
}


@Test
public void Test2()
{
	
}
}
