package multiThreadParallel;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//Returning browser instance for mentioned browser
public class BrowserFactory {

static WebDriver driver=null;

	//Launch ur browser as per browser reqt from user
	public static WebDriver getBrowser(String browser)
	{
		
		switch(browser)
		{
		case "chrome":
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
     		driver=new ChromeDriver();    					
			break;
			
		case "default":
			
			break;	
		
		}
         driver.manage().window().maximize();
         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
         driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);	
         return driver;
	}
	
	
	
}
