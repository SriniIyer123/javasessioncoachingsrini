package multiThreadParallel;

import org.openqa.selenium.WebDriver;

//create  batch file-property to Jenkins, create a executable jar(Selenium Grid)
public class ParallelExecution {// extends Thread
//Singleton Design Pattern=> Java Design Pattern
//Always there will be one instance of the class 

	//start=> run() method
	
	/*@Override
	public void run()
	{
		
	}*/
	
	WebDriver driver=null;
	//Multi-threading needs to be achieved- ThreadLocal
	ThreadLocal<WebDriver> driverVal=new ThreadLocal<WebDriver>();

	//ArrayList<String> arrlist=new ArrayList<String>();
	static ParallelExecution parallel=new ParallelExecution();
    //Constructor should be private
	private ParallelExecution()
	{
		
	}
	
		
	public static ParallelExecution getInstance()
	{
		return parallel;
	}
	
	public void SetDriver(String browserName)
	{
		driver=BrowserFactory.getBrowser(browserName);
		driverVal.set(driver);		
	}
	
	public WebDriver getDriver()
	{
		return driverVal.get();
	}
	

}
