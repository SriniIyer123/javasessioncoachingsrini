package miscellaneous;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutoItDemo {

	

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//resources//chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		driver.get("http://softwaretestingplace.blogspot.com/2015/10/sample-web-page-to-test.html");

		driver.findElement(By.xpath("//*[@id='uploadfile']")).click();
		try {
			Runtime.getRuntime().exec("C:\\Users\\Srinivasan\\Desktop\\UploadFileScript.exe");
		    
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
