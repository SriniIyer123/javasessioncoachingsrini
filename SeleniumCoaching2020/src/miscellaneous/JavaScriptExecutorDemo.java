package miscellaneous;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JavaScriptExecutorDemo {


	public static void main(String[] args) {
		
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//resources//chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		//driver.get("https://www.guru99.com/");
		driver.get("https://www.google.com/");
		JavascriptExecutor js=(JavascriptExecutor)driver;		
		
		//js.executeScript("document.getElementById('gsc-i-id2').value='Selenium Testing';");
		//js.executeScript("window.scrollBy(0,600);");
		WebElement SignIn=driver.findElement(By.linkText("Sign in"));
		js.executeScript("arguments[0].click();",SignIn);
	
	}

}
