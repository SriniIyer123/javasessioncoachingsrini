package miscellaneous;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class SikuliDemo {

	//driver.findElement(By.xpath(""));
    //Screen(play role like a driver) screenobject  Pattern(identifies element)
	public static void main(String[] args) {
		WebDriver driver;
		try {
		DesiredCapabilities desiredcap=DesiredCapabilities.chrome();
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//resources//chromedriver.exe");
		desiredcap.setCapability("ignoreZoomSetting", true);
		driver = new ChromeDriver(desiredcap);			
		
		/*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//resources//chromedriver.exe");
		driver=new ChromeDriver();*/
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		driver.get("https://www.google.com");
				
		Screen screen=new Screen();
			
		String filepath=System.getProperty("user.dir");//+"//imagesSikuli";
		System.out.println("Filepath:"+filepath);
		Pattern Googlelogo=new Pattern(filepath+"//Googlelogo2.png");
		Pattern GoogleSearchTextbox=new Pattern(filepath+"//searchtxtbox.png");		
		Pattern Searchdropdownlist=new Pattern(filepath+"//drpdownlist2.png");
        screen.exists(Googlelogo);
		screen.type(GoogleSearchTextbox,"Java");
		screen.click(Searchdropdownlist);
		System.out.println("Page title of redirected search results page:"+driver.getTitle());			
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
