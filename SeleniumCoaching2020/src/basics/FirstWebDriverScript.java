package basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import config.ConstantsFile;
import testng.TestNGBasics;

public class FirstWebDriverScript{// extends TestNGBasics{
WebDriver driver;
	public FirstWebDriverScript() {
		// TODO Auto-generated constructor stub
	}
	//WebDriver=>Interface
	//Classes implementing above interface are ChromeDriver FirefoxDriver OperaDriver InternetExplorerDriver

	@AfterMethod//After a Test method(Method) is executed , this method gets executed.
	public void afterMethod()
	{
		Reporter.log("In AfterMethod");
		System.out.println("In AfterMethod");
		driver.quit();
	}
	
@BeforeMethod//Before a Test method(Method) is executed , this method gets executed.
public void b4method()
{
	//Launching the Google Application in launched Chrome browser
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//resources//chromedriver.exe");
	driver=new ChromeDriver();
	driver.get(ConstantsFile.applicationUrl);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	Reporter.log("In B4Method");
	System.out.println("In B4Method");
}

	@Test(enabled=true)
	@Parameters({"username","SearchCriteria"})
	public void runScript(String username, String SearchCriteria)
	{
		System.out.println("In FirstWebDriverScript class method");
		Reporter.log("In FirstWebDriverScript class method");
		
		WebElement TextBoxelement=null;
		System.out.println("User:"+username +" has logged in");
		TextBoxelement=driver.findElement(By.xpath("//*[@title='Search']"));
		//SearchButtonelement=formLocator("Google_Search_SearchButton_name");
		TextBoxelement.sendKeys(SearchCriteria);//entering the value in searchtextbox
		driver.findElement(By.name("btnK")).click();
		System.out.println("Page Title:"+driver.getTitle());		
	}
	
	
	public static void main(String[] args) 
	{
		//1.set the property to indicate the chromedriver
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//resources//chromedriver.exe");//ie //for >3 selenium standalone server, .firefox
//directly launch browser
		ChromeDriver driver=new ChromeDriver();//launching an instance of chrome browser
		driver.get("https://www.naukri.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);//Pageload timeout exception if it exeeds beyond this
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);//implicit wait=> by default for all actions for any element interactions
        Wait explwait=new WebDriverWait(driver,5);//explicitly wait for an element or some condition
       //explwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='id1']")));
      	driver.close();//close current active window only
		driver.quit();//close all open windows for the current session and of launched browser
		//FirefoxDriver driver=new FirefoxDriver();
	}

}
