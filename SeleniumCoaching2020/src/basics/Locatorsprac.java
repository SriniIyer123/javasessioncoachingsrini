package basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

//FluentWait,ExplicitWait,ImplicitWait

public class Locatorsprac {
	static WebDriver driver;
	public static void waitSometime(long waittime)
	{
		try {
			Thread.sleep(waittime);//driver.wait(waittime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public static void main(String[] args) 
	{
		
	  try {	
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
	    driver=new ChromeDriver();
	    driver.manage().window().maximize();
	  //  waitSometime(1000);
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.get("https://www.seleniumeasy.com/test/");	
		//waitSometime(1000);
		//Locators: id,name,classname,cssSelector,linkText,partiallinkText,xpath(Abs,relative), other xpath methods, Xpath Axes
        //Input Forms //*[@class='nav navbar-nav']//a[contains(text(),'Input Forms')]
		//driver.findElement(By.tagName("a"));//img div input radio select
	   driver.findElement(By.xpath("//*[@class='nav navbar-nav']//a[contains(text(),'Input Forms')]")).click();
	   //waitSometime(1000);
	   driver.findElement(By.xpath("//ul[@class='dropdown-menu']//a[contains(text(),'Simple Form Demo')]")).click();
	   //waitSometime(1000);
	   System.out.println("Page Title:"+driver.getTitle());//match=>Pass , if not match then testcase step will Fail
	   //driver.quit();//close all open windows of current session
	   
	   
	   WebElement Entermessage= driver.findElement(By.id("user-message"));//find element by id
	   Entermessage.sendKeys("We are learning Selenium");
	   waitSometime(1000);
	   System.out.println("Value entered by the user in Enter message field:"+Entermessage.getAttribute("value"));
	   
	   WebElement ShowMessage=driver.findElement(By.xpath("//*[@onclick='showInput();']"));
	   System.out.println("Label of Show Message field:"+ShowMessage.getText());	   
	   ShowMessage.click();//if its clicked it shows a label below the button
	   
	   WebElement Messageresult=driver.findElement(By.id("display"));
	   boolean messgshown=Messageresult.isDisplayed();
	   System.out.println("Message is shown or not:"+messgshown);
	   System.out.println("Message seen after clicking on ShowMessage button is:"+Messageresult.getText());
	   
	   Assert.assertTrue(messgshown==true);//abruptly stop in between
	   Assert.assertEquals(Entermessage.getAttribute("value"),Messageresult.getText());
	   
	   
	   WebElement Entera=driver.findElement(By.cssSelector("#sum1"));
	   WebElement Enterb=driver.findElement(By.xpath("//form[@id='gettotal']/div[2]/input[@placeholder='Enter value']"));
	   
	   Entera.sendKeys("23");
	   Enterb.sendKeys("23");
	   
	   //driver.quit();
	  }
	  catch(Exception e)
	  {
		  System.out.println("Exception caught in main method of class:Locatorsprac"+e.getMessage());
	  }
	  catch(AssertionError asserterr)
	  {
		  System.out.println("AssertionError caught in main method of class:Locatorsprac"+asserterr.getMessage());
	  }
	}

}
