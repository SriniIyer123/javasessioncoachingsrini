package basics;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

//Select Dropdown
public class MiscellaneousElements 
{
	
	static WebDriver driver;
	
	//create a reusable utility method, use xpaths for selecting a value from a dropdown when ul->li
	
	
	//method to select BootStrapDatePicker-assignment-Select Date
	//div[contains(text(),'Date Example :')]/parent::div//span[@class='input-group-addon'] icon and then select from calendar
	
	
	//when select, option
	public static WebElement selectDropdown(String xpdropdown,String value)
	{
		WebElement dropdown=driver.findElement(By.xpath(xpdropdown));
		Select selectValue=new Select(dropdown);//tag name is select
		String xpathVal=xpdropdown +"//option"+"[@value='"+value+"']";//dynamic xpath
		selectValue.selectByValue(value);
		System.out.println("Does this select dropdown allows multi-select?"+selectValue.isMultiple());
		
	    WebElement selectedValue=driver.findElement(By.xpath(xpathVal));
		return selectedValue;
	}
	
	//click- reusable utility method to click on an element
	
	public static void clickElement(WebElement element)
	{
		element.click();
	}
	

	
	
	public static void main(String[] args) {
		try {	
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
		    driver=new ChromeDriver();
		    driver.manage().window().maximize();
		  //  waitSometime(1000);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.get("https://www.seleniumeasy.com/test/");
			WebElement InputForms=driver.findElement(By.linkText("Input Forms"));
			clickElement(InputForms);
			
			WebElement SelectDropdownList=driver.findElement(By.xpath("//ul[@class='nav navbar-nav']//a[text()='Select Dropdown List']"));
			clickElement(SelectDropdownList);
					
			System.out.println("Page title:"+driver.getTitle());//Selenium Easy Demo - Automate All Scenarios
			
			String drpdownxPath="//select[@id='select-demo']";
			//WebElement dropdown=driver.findElement(By.xpath("//select[@id='select-demo']"));//dropdown xpath 
			String value="Monday";
			WebElement elementSelected=selectDropdown(drpdownxPath,value);
			WebElement result=driver.findElement(By.xpath("//p[@class='selected-value']"));
			System.out.println("Is result displayed?"+result.isDisplayed());
			System.out.println("Result getText:"+result.getText());
			System.out.println("Is Element selected in dropdown?"+elementSelected.isSelected());
		}
		 catch(Exception e)
		  {
			  System.out.println("Exception caught in main method of class:MiscellaneousElements"+e.getMessage());
		  }
		  catch(AssertionError asserterr)
		  {
			  System.out.println("AssertionError caught in main method of class:MiscellaneousElements"+asserterr.getMessage());
		  }
		
		
	}

}
