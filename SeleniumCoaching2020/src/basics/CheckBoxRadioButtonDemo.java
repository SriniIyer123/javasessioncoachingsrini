package basics;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBoxRadioButtonDemo {

	static WebDriver driver;
	public static void waitSometime(long waittime)
	{
		try {
			Thread.sleep(waittime);//driver.wait(waittime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public static void main(String[] args) {
		try {	
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
		    driver=new ChromeDriver();
		    driver.manage().window().maximize();
		  //  waitSometime(1000);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.get("https://www.seleniumeasy.com/test/");	
			driver.findElement(By.linkText("Input Forms")).click();
			
			WebElement Checkboxdemo=driver.findElement(By.partialLinkText("Checkbox"));
			Checkboxdemo.click();
					
			System.out.println("Page title of Checkbox Demo is:"+driver.getTitle());
			WebElement Checkbox=driver.findElement(By.id("isAgeSelected"));
			Checkbox.click();
			System.out.println("Is Checkbox selected?:"+Checkbox.isSelected());
			
			//*[text()='Multiple Checkbox Demo']//following::input[1]
			//driver.findElement(By.xpath("//*[text()='Multiple Checkbox Demo']//following::input[1]")).click();//Option1
			//driver.findElement(By.xpath("//*[text()='Multiple Checkbox Demo']//following::input[2]")).click();//Option1
			List<WebElement> checkboxOptions=driver.findElements(By.xpath("//*[text()='Multiple Checkbox Demo']//following::label"));
			//for-each loop
			//foreach method
			
			Iterator<WebElement> checkboxItr=checkboxOptions.iterator();
			while(checkboxItr.hasNext())
			{
				WebElement element=checkboxItr.next();
				System.out.println("GetText:"+element.getText());
				System.out.println("Value:"+element.getAttribute("value"));
				if(element.getText().equals("Option 3")) {				
					element.click();
					System.out.println("Selection found for "+element.getText());
					break;
				}
				else {
					System.out.println("Selection not yet found for "+element.getText());
					continue;
				}
			}			
			System.out.println("Came out of while loop");
			
			driver.findElement(By.linkText("Input Forms")).click();
			driver.findElement(By.xpath("//ul[@class='dropdown-menu']//a[text()='Radio Buttons Demo']")).click();
			System.out.println("Page title of Radiobutton demo is:"+driver.getTitle());
			
			List<WebElement> radioOptions=driver.findElements(By.xpath("//*[text()='Radio Button Demo']//following-sibling::div[1]/label"));
			//for-each loop
			//foreach method
			
			Iterator<WebElement> radiobtnItr=radioOptions.iterator();
			while(radiobtnItr.hasNext())
			{
				WebElement element=radiobtnItr.next();
				System.out.println("GetText:"+element.getText());
				if(element.getText().equals("Male")) {				
					element.click();
					System.out.println("Selection found for "+element.getText());
					break;
				}
				else {
					System.out.println("Selection not yet found for "+element.getText());
					continue;
				}
			}
			System.out.println("Came out of while loop");
			driver.findElement(By.id("buttoncheck")).click();
			WebElement outputlabel=driver.findElement(By.xpath("//*[@id='buttoncheck']/parent::p/following-sibling::p"));
			if(outputlabel.isDisplayed())
			{
				System.out.println("OutputLabelText:"+outputlabel.getText());
			}
					
		}
		 catch(Exception e)
		  {
			  System.out.println("Exception caught in main method of class:Locatorsprac"+e.getMessage());
		  }
		  catch(AssertionError asserterr)
		  {
			  System.out.println("AssertionError caught in main method of class:Locatorsprac"+asserterr.getMessage());
		  }
	

	}

}
