package basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

//xpath axes: https://www.guru99.com
//linkText,partialLinkText
//Click on some hyperlink, and then identify elements using XpathAxes
//*[text()='SAP Basis']/ancestor::div[@class='featured-box']

//ancestor,descendant, self, parent, child, following-sibling, preceding-sibling
//xpath methods:contains, starts-with,and, or

public class XpathAxesPrac 
{
	static WebDriver driver;
	public static void waitSometime(long waittime)
	{
		try {
			Thread.sleep(waittime);//driver.wait(waittime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public static void main(String[] args)
	{
		try {	
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
		    driver=new ChromeDriver();
		    driver.manage().window().maximize();
		  //  waitSometime(1000);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.get("https://www.seleniumeasy.com/test/");	
			driver.findElement(By.linkText("Table")).click();
			
			WebElement TablePagination=driver.findElement(By.partialLinkText("Pagination"));
			TablePagination.click();
					
			System.out.println("Page title of Table Pagination is:"+driver.getTitle());
		    String getTxt=driver.findElement(By.xpath("//*[@id='myTable']/descendant::tr[3]/td[5]")).getText();
		    System.out.println("Get text of 3rd row and 4th col:"+getTxt);		
		}
		 catch(Exception e)
		  {
			  System.out.println("Exception caught in main method of class:Locatorsprac"+e.getMessage());
		  }
		  catch(AssertionError asserterr)
		  {
			  System.out.println("AssertionError caught in main method of class:Locatorsprac"+asserterr.getMessage());
		  }
		

	}

}
