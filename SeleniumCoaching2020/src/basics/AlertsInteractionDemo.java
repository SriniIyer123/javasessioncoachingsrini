package basics;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertsInteractionDemo {

	static WebDriver driver;
	public static void waitSometime(long waittime)
	{
		try {
			Thread.sleep(waittime);//driver.wait(waittime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public static void main(String[] args){
		try {	
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\resources\\chromedriver.exe");
		    driver=new ChromeDriver();
		    driver.manage().window().maximize();
		  //  waitSometime(1000);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.get("http://demo.guru99.com/test/delete_customer.php");	
			driver.findElement(By.name("cusid")).sendKeys("3444");
			driver.findElement(By.name("submit")).click();
			Alert trytoDelete=driver.switchTo().alert();
			System.out.println("Alert Text:"+trytoDelete.getText());
			trytoDelete.dismiss();
			/*trytoDelete.accept();
			Alert deletionConfirmation=driver.switchTo().alert();
			System.out.println("Alert Text:"+deletionConfirmation.getText());
			deletionConfirmation.accept();*/
			
		}
		 catch(Exception e)
		  {
			  System.out.println("Exception caught in main method of class:Locatorsprac"+e.getMessage());
		  }
		  catch(AssertionError asserterr)
		  {
			  System.out.println("AssertionError caught in main method of class:Locatorsprac"+asserterr.getMessage());
		  }
	

	}

}
