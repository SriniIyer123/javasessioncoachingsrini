package basics;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

//< 3 then we dont have to setup the path for gecko driver for Firefox
public class WindowsInteractionDemo {
   static WebDriver driver;
//setting up browser
	
    static void setUp(String browser,String application)//Chrome chrome CHROME
	{
    	browser=browser.toLowerCase();
    	switch(browser)
    	{
    	case "chrome":
    		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
    		driver=new ChromeDriver();    		    	
    		break;
    		
    	case "firefox":	
    	   
    	case "ie":
    		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "//resources//chromedriver.exe");
    		driver=new InternetExplorerDriver();
    		break;
    	
    	default:
    	      System.out.println("Incorrect browsername");
    	      break;
    	}
    	System.out.println("BrowserName:"+browser);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    	driver.get(application);    	
	}
	
    static void quitallApp()
    {
    	driver.quit();
    }
	
    //Go to child windows. get the title, print address bar, close them and come back to parent window
    public static void testwindowsInteraction()
    {
    	System.out.println("Page Title:"+driver.getTitle());
    	System.out.println("Page URL:"+driver.getCurrentUrl());    	
    	String parentWindow=driver.getWindowHandle();//=> naukri window currently focussed window unique identifier PARENT WINDOW Handle
    	Set<String>allWindows=driver.getWindowHandles();//Croma, RBS, Naukri main application
    	Iterator<String>allWindowsItr=allWindows.iterator();
    	
    	while(allWindowsItr.hasNext())
    	{
    		String subWindow=allWindowsItr.next();//naukri croma RBS
    		if(!(subWindow.equals(parentWindow)))// croma RBS
    		{
    			
    			driver.switchTo().window(subWindow);
    			System.out.println("SubWindow title:"+driver.getTitle());
    			System.out.println("Subwindow URL:"+driver.getCurrentUrl());
    			//System.out.println("Is Croma element displayed?"+driver.findElement(By.xpath("//img[@alt='Croma']")).isDisplayed());
    			driver.close();
    		}    				
    	}
    	driver.switchTo().window(parentWindow);
    	System.out.println("ParentWindow title:"+driver.getTitle());
		System.out.println("ParentWindow URL:"+driver.getCurrentUrl());
		driver.close();  	   	
    }
	
	public static void main(String[] args) {
		setUp("chrome","https://www.naukri.com");
		testwindowsInteraction();
		quitallApp();
	}

}
