package basics;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;

//By.tagName("iframe")=> didnt give any count, take any other example and get back
public class FrameInteractions {

	   static WebDriver driver;
	 //setting up browser
	 	
	     static void setUp(String browser,String application)//Chrome chrome CHROME
	 	{
	     	browser=browser.toLowerCase();
	     	switch(browser)
	     	{
	     	case "chrome":
	     		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "//resources//chromedriver.exe");
	     		driver=new ChromeDriver();    		    	
	     		break;
	     		
	     	case "firefox":	
	     	   
	     	case "ie":
	     		System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "//resources//chromedriver.exe");
	     		driver=new InternetExplorerDriver();
	     		break;
	     	
	     	default:
	     	      System.out.println("Incorrect browsername");
	     	      break;
	     	}
	     	System.out.println("BrowserName:"+browser);
	 		driver.manage().window().maximize();
	 		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	 		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
	     	driver.get(application);    	
	 	}
	
	    static public void interactFrame()
	    {
	    	//driver.switchTo().frame(0);
	    	//driver.switchTo().frame("frame1");//name of the frame
	    	//WebElement Topmostframe=driver.findElement(By.xpath(""));	 
	    	
	    driver.switchTo().frame("a077aa5e");//Only one frame
	    System.out.println("Is this element displayed?"+driver.findElement(By.xpath("//img[@src='Jmeter720.png']")).isDisplayed());	    	
	        
	    //frame iframe but dont be confused with frameset(this is not a frame- its a collection of frames)
	    //frame1(parent frame)=>frame2(child frame)[Nested Frame]=>element
	    driver.switchTo().defaultContent();//this means going back to default section=> this we need to do after we switch to a frame
	    WebElement outerFrame=driver.findElement(By.xpath("//iframe[@title='Primis Player Videos']"));	    			
	    driver.switchTo().frame(outerFrame);//a img input iframe select radio li ul- examples of tag names
	    WebElement innerFrame=driver.findElement(By.xpath("//iframe[contains(@id,'Video-iFrame-SekindoSPlayer')]"));
	    driver.switchTo().frame(innerFrame);
	    System.out.println("Is embedded video enabled?"+driver.findElement(By.xpath("//body/div[@id='skinContainerDiv']//*[@id='transparentInner']")).isEnabled());
  	      
	    List<WebElement> iframesListInitial=driver.findElements(By.tagName("iframe"));
	    System.out.println("iframes count initial:"+iframesListInitial.size());
	    driver.switchTo().defaultContent();
   		driver.switchTo().frame(driver.findElement(By.id("flow_close_btn_iframe")));//Close frame	    
	    driver.findElement(By.id("closeBtn")).click();
	    //flow_close_btn_iframe
	    List<WebElement> iframesListFinal=driver.findElements(By.tagName("iframe"));//above advertisement window will not be shown
	    System.out.println("iframes count final:"+iframesListFinal.size());
	    
	    }
	     
	     
	     
	public static void main(String[] args) {	
		setUp("chrome","http://demo.guru99.com/test/guru99home/");
		interactFrame();
	}

}
