package poi_excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelHelper {
	static Object[][] ExcelTestData;
	int rowCount,colCount=0;
	//Workbook Sheet Row Cell
	//XSSFWorkbook =>.xlsx and HSSFWorkbook=>.xls//Testcases
	Workbook workbook;
	Sheet sheet;
	Row row;
	Cell cell;
	
	public static Object getCellData(int row, int col)
	{
		return ExcelTestData[row][col];		
	}
	
	public void printExcelData()
	{
		System.out.println("ExcelValues:");
		for(int i=1;i<ExcelTestData.length;i++)//row number is starting from 1
		{
			for(int j=0;j<ExcelTestData[0].length;j++)//cols starting from 0
			{
				System.out.print(ExcelTestData[i][j]+" ");
			}
			System.out.println();
		}		
		
	}
	
	public void readfromExcel(String RelFilepath)
	{	
		
		try {
		String filepath=System.getProperty("user.dir")+RelFilepath;// .xlsx
		String filepathobjs[]=filepath.split("/");
		String ExcelFileName=filepathobjs[filepathobjs.length-1];
		System.out.println("ExcelFileName:"+ExcelFileName);
		
		File file=new File(filepath);		
		FileInputStream fip=new FileInputStream(file);
		String fileext=ExcelFileName.substring(ExcelFileName.indexOf('.')+1);
		System.out.println("FileExtension:"+fileext);
		
		if(fileext.equals("xlsx"))
		{
			workbook=new XSSFWorkbook(fip);	
			sheet=workbook.getSheet("Testcases");
			rowCount=(sheet.getLastRowNum()-sheet.getFirstRowNum())+1;//7
			row=sheet.getRow(3);
			colCount=row.getLastCellNum();
			System.out.println("Row:"+rowCount+" Col:"+colCount);
			ExcelTestData=new Object[rowCount][colCount];
			
			for(int i=1;i<rowCount;i++)//rows
			{
				row=sheet.getRow(i);//1st row onwards data is stored				
				for(int j=0;j<colCount;j++)
				{
					cell=row.getCell(j);
					if(Cell.CELL_TYPE_NUMERIC==cell.getCellType())
					{
						ExcelTestData[i][j]=cell.getNumericCellValue();
					}
					else if(Cell.CELL_TYPE_BOOLEAN==cell.getCellType())
					{
						ExcelTestData[i][j]=cell.getBooleanCellValue();
					}
					else if(Cell.CELL_TYPE_STRING==cell.getCellType())
					{
						ExcelTestData[i][j]=cell.getStringCellValue();
					}					
				}//cols				
				
			}
			
			
		}
		else if(fileext.equals("xls"))
		{
			workbook=new HSSFWorkbook(fip);	
			sheet=workbook.getSheet("Testcases");
			
		}		
		
		}catch(FileNotFoundException e) {
			System.out.println("FileNotFoundException caught in readfromExcel:"+e.getMessage());
		}
		catch(Exception e) {
			System.out.println("Exception caught in readfromExcel:"+e.getMessage());

		}
		
	}
	
	public void writetoExcel(int rowNum, int colNum, Object objVal)//5th ind:PASS/FAIL
	{		
		FileOutputStream fop;
		File file;
		FileInputStream fip;
		try {
		String filepath=System.getProperty("user.dir")+"//resources//Testdemo.xlsx";// .xlsx
		String filepathobjs[]=filepath.split("/");
		String ExcelFileName=filepathobjs[filepathobjs.length-1];
		System.out.println("ExcelFileName:"+ExcelFileName);
		boolean Stringcont=false;
		boolean NumCont=false;
		boolean BooleanCont=false;
		
		file=new File(filepath);		
		fip=new FileInputStream(file);
		String fileext=ExcelFileName.substring(ExcelFileName.indexOf('.')+1);
		System.out.println("FileExtension:"+fileext);
		
		if(fileext.equals("xlsx"))
		{
			workbook=new XSSFWorkbook(fip);	
			sheet=workbook.getSheet("Testcases");
			rowCount=(sheet.getLastRowNum()-sheet.getFirstRowNum())+1;//7
			row=sheet.getRow(0);
			colCount=row.getLastCellNum();			
			
			row=sheet.createRow(rowCount);//at 7th index since this is the next row after 6th index			
      		for(int j=0;j<colCount;j++)
			{
				cell=row.createCell(j);
				//cell.setCellVa
				cell.setCellValue((String)objVal);
			}				
					
			row=sheet.getRow(rowNum);
			cell=row.getCell(colNum);				
				
			if(Cell.CELL_TYPE_STRING==cell.getCellType())
			{
				cell.setCellValue((String)objVal);
				Stringcont=true;				
			}
			else if(Cell.CELL_TYPE_BOOLEAN==cell.getCellType())
			{
				cell.setCellValue((boolean)objVal);
				BooleanCont=true;
			}
			else if(Cell.CELL_TYPE_NUMERIC==cell.getCellType())
			{
				cell.setCellValue((double)objVal);
				NumCont=true;
			}
			if(Stringcont)
			 System.out.println("Check if cell data is updated:"+cell.getStringCellValue());
			else if(BooleanCont)
			 System.out.println("Check if cell data is updated:"+cell.getBooleanCellValue());
			else if(NumCont)
			 System.out.println("Check if cell data is updated:"+cell.getNumericCellValue());
			
			fop=new FileOutputStream(file);
			workbook.write(fop);			
			fip.close();
			fop.close();
			//file.delete();			
		}		
		}catch(FileNotFoundException e) {
			System.out.println("FileNotFoundException caught in readfromExcel:"+e.getMessage());
		}
		catch(IOException e)
		{
			System.out.println("IOException caught in readfromExcel:"+e.getMessage());
		}
		catch(Exception e) {
			System.out.println("Exception caught in readfromExcel:"+e.getMessage());
		}
		finally {
			System.out.println("Finally clause");
		}
		
		
		
	}
	
	
	public ExcelHelper() {
		// TODO Auto-generated constructor stub
	}

	/*public static void main(String[] args) {
		ExcelHelper excelT=new ExcelHelper();
		excelT.readfromExcel();
		excelT.printExcelData();
		System.out.print("Cell Data:"+excelT.getCellData(6,3));//row num=6 col num=4
		excelT.writetoExcel(3, 5, "PASS");//rownum starts from 0 so rownum=2=> 3 and colnum:5 => 6
		excelT.readfromExcel();
		excelT.printExcelData();
	}
*/
}
