package seleniumgrid;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class SeleniumGridDemo {
WebDriver driver;
	
//batch file => how to create a batch and how to run batch file(Automated)
	public SeleniumGridDemo() {
		// TODO Auto-generated constructor stub
	}
	
	//launch browser using Selenium Grid=> Pre-requisite
	
	@BeforeSuite
	public void launchSeleniumGrid()
	{
		//Steps mentioned here to run batch file-1 for Hub 
		//Steps mentioned here to run batch file-2 for Node 
		//Now our basic selenium grid setup is ready to use for our automation test cases.
	}
	
	@BeforeMethod
	public void b4Suite()
	{
		try {
		String nodeURL="http://192.168.140.1:4444/wd/hub";		
		DesiredCapabilities cap=DesiredCapabilities.chrome();
		cap.setPlatform(Platform.WIN8_1);
		driver=new RemoteWebDriver(new URL(nodeURL),cap);		
		} catch (MalformedURLException e) {
			System.out.println("Exception caught in b4Suite method:"+e.getMessage());
		}
				
	}
	
	
	@Test
	public void performTest()
	{
		driver.get("https://www.gmail.com");
		System.out.println("Title:"+driver.getTitle());	
	}

	@Test
	public void performTest2()
	{
		driver.get("https://www.google.com");
		System.out.println("Title:"+driver.getTitle());		
	}
	
	@Test
	public void performTest3()
	{
		driver.get("https://www.facebook.com");
		System.out.println("Title:"+driver.getTitle());		
	}
}
