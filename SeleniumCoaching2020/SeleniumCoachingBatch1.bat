cd C:\Users\Srinivasan\eclipse-workspace\SeleniumCoaching2020
@echo off
set logfile=C:\Users\Srinivasan\eclipse-workspace\SeleniumCoaching2020\exec.log
echo starting app.exe at %date% %time% >> %logfile%
app.exe -options oas >> %logfile%
java -cp bin;lib/* org.testng.TestNG testng.xml
pause